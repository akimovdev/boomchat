package com.akimovdev.boomchat.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;

import com.akimovdev.boomchat.components.loader.DataLoader;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.components.loader.ImageLoader;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.utils.PoolTransporter;


public class LoadFileService extends IntentService {

    private static final String TAG = "LOAD_FILE_SERVICE";

    private static final String ACTION_LOAD = "com.akimovdev.messenger.services.LOAD";
    private static final String ACTION_LOAD_VIDEO = "com.akimovdev.messenger.services.ACTION_LOAD_VIDEO";
    private static final String EXTRA_FILE_URI = "com.akimovdev.messenger.services.EXTRA_FILE_URI";

    private static final String EXTRA_POOL_USER_KEY = "com.akimovdev.messenger.services.extra.USER_POOL_KEY";
    public static final String EXTRA_REQUEST_CODE = "com.akimovdev.messenger.services.extra.REQUEST_CODE";

    private User user;
    private ResultReceiver receiver;
    private DataLoader dataLoader;

    public LoadFileService() {
        super("LoadFileService");
    }

    public static void startActionLoadImage(Context context, Uri uri, User user, int requestCode, ServiceReceiver receiver) {
        startAction(context, uri, user, receiver, ACTION_LOAD, requestCode);
    }

    public static void startActionLoadVideo(Context context, Uri uri, User user, ServiceReceiver receiver) {
        startAction(context, uri, user, receiver, ACTION_LOAD_VIDEO, -1);
    }

    private static void startAction(Context context, Uri uri, User user, ServiceReceiver receiver, String action, int requestCode) {

        PoolTransporter.getInstance().push(Constants.Extras.USER, user);

        Intent intent = new Intent(context, LoadFileService.class);
        intent.setAction(action);
        intent.putExtra(EXTRA_FILE_URI, uri);
        intent.putExtra(EXTRA_POOL_USER_KEY, Constants.Extras.USER);
        intent.putExtra(EXTRA_REQUEST_CODE, requestCode);
        intent.putExtra(Constants.Extras.RESULT_RECEIVER, receiver);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        receiver = intent.getParcelableExtra(Constants.Extras.RESULT_RECEIVER);

        final String action = intent.getAction();
        final Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
        user = (User) PoolTransporter.getInstance().pop(intent.getStringExtra(EXTRA_POOL_USER_KEY));

        if (ACTION_LOAD.equals(action)) {
            int requestCode = intent.getIntExtra(EXTRA_REQUEST_CODE, -1);
            handleActionLoad(fileUri, requestCode);
        }
    }

    private void handleActionLoad(Uri imageUri, int requestCode) {
        dataLoader = ImageLoader.newImageLoader(this, imageUri, requestCode, user, new ImageLoader.LoadListener() {

            @Override
            public void onCreateAvatar(String downloadUrl, int requestCode) {

                String downloadUrlStr = downloadUrl.toString();

                Bundle bundle = new Bundle();
                bundle.putString(Constants.Extras.IMAGE_PATH, downloadUrlStr);
                bundle.putInt(EXTRA_REQUEST_CODE, requestCode);
                receiver.send(Constants.RESULT_RECEIVER_AVATAR_CREATE, bundle);
            }
        });

        dataLoader.actionLoad();
    }
}

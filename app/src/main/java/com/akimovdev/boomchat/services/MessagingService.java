package com.akimovdev.boomchat.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.activities.MainActivity;
import com.akimovdev.boomchat.ui.activities.NotificationActivity;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MessagingService extends FirebaseMessagingService {
    private static final String TAG = "MESSAGING_SERVICE";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if ( User.getCurrentUser(this) != null && (MessengerApplication.getInstance() == null || !MessengerApplication.getInstance().isChatIn()) )
                parseData(remoteMessage.getData());
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
    }

    private void parseData(Map<String,String> data) {
        String displayName = data.get("displayName");
        String body = data.get("body");
        String chatKey = data.get("chatKey");
        String chatLastMessageKey = data.get("lastMessageKey");
        String contactUID = data.get("contactUID");
        String contactPhoto = data.get("contactPhoto");

        User contactUser = new User();
        Chat chat = new Chat();

        if (contactPhoto != null)
            contactUser.setPhotoUri(Uri.parse(contactPhoto));

        contactUser.setUid(contactUID);
        contactUser.setDisplayName(displayName);

        chat.setKey(chatKey);
        chat.setLastMessageKey(chatLastMessageKey);

        //sendNotification(displayName, body, chatKey, contactUser);
        sendNotification(displayName, body, chat, contactUser);
    }

    private void sendNotification(String displayName, String messageBody, Chat chat, User contactUser) {

        loadData(chat.getKey());

        Class clazz = MainActivity.isRun() ? NotificationActivity.class : MainActivity.class;

        Intent intent = new Intent(this, clazz);
        intent.putExtra(Constants.Extras.CHAT, chat);
        intent.putExtra(Constants.Extras.CONTACT_USER, contactUser);

        //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentTitle(displayName)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        //notificationManager.notify(chat.getKey().hashCode(), notificationBuilder.build());
        notificationManager.notify(Constants.NOTIFICATION_CHAT_ID, notificationBuilder.build());
    }

    private void loadData(final String chatKey) {
        FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DataBase.MESSAGES)
                .child(chatKey).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                FirebaseDatabase.getInstance()
                        .getReference()
                        .child(Constants.DataBase.MESSAGES)
                        .child(chatKey).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}

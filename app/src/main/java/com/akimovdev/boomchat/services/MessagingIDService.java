package com.akimovdev.boomchat.services;

import android.util.Log;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.HashMap;
import java.util.Map;

public class MessagingIDService extends FirebaseInstanceIdService {

    private final static String TAG = "MESSAGING_ID_SERVICE";


    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(final String token) {

        RequestQueue queue = Volley.newRequestQueue(this);
        String url = Constants.URL_SERVER + "sendtoken.php";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //mTextView.setText("Response is: "+ response.substring(0,500));
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //mTextView.setText("That didn't work!");
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String androidId = MessengerApplication.getAndroidID(getContentResolver());
                //String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

                Map<String, String>  params = new HashMap<String, String>();

                params.put("token", token);
                params.put("duid", androidId);

                return params;
            }
        };
        // Add the request to the RequestQueue.
        queue.add(stringRequest);

    }
}

package com.akimovdev.boomchat.services;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

public class ServiceReceiver extends ResultReceiver {

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }

    private Receiver receiver;

    public void setReceiver(Receiver receiver) {
        this.receiver = receiver;
    }

    public ServiceReceiver(Handler handler) {
        super(handler);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (receiver != null) {
            receiver.onReceiveResult(resultCode, resultData);
        }
    }

    public boolean isSetReceiver() {
        return receiver != null;
    }
}

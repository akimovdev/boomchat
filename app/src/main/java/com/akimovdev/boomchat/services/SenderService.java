package com.akimovdev.boomchat.services;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.Context;
import android.net.Uri;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.Message;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


public class SenderService extends Service {

    private static final String TAG = "SenderService";

    private static final String ACTION_SEND_MSG = "com.akimovdev.messenger.services.action.SEND_MSG";

    private static final String EXTRA_CHAT_KEY = "com.akimovdev.messenger.services.extra.CHAT_KEY";
    private static final String EXTRA_MESSAGE = "com.akimovdev.messenger.services.extra.MESSAGE";
    private static final String EXTRA_CONTACT_USER = "com.akimovdev.messenger.services.extra.CONTACT_USER";

    private static final int SOCKET_TIMEOUT_MS = 2000;

    private ResultReceiver receiver;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    public static void startActionSendMessage(Context context, String chatKey, Message message, User contactUser, User currentUser, ServiceReceiver receiver) {

        Intent intent = new Intent(context, SenderService.class);
        intent.setAction(ACTION_SEND_MSG);
        intent.putExtra(EXTRA_CHAT_KEY, chatKey);
        intent.putExtra(Constants.Extras.RESULT_RECEIVER, receiver);

        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(EXTRA_CONTACT_USER, contactUser);

        context.startService(intent);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand");
        if (intent != null) {
            receiver = intent.getParcelableExtra(Constants.Extras.RESULT_RECEIVER);

            Message message = intent.getParcelableExtra(EXTRA_MESSAGE);
            User currentUser = User.getCurrentUser(this);
            User contactUser = intent.getParcelableExtra(EXTRA_CONTACT_USER);
            String chatKey = intent.getStringExtra(EXTRA_CHAT_KEY);

            checkForNotNullable(message, chatKey, currentUser, contactUser);

            StringRequest request = notificationRequest(message, chatKey, currentUser, contactUser);

            android.os.Message msg = mServiceHandler.obtainMessage();
            msg.obj = request;
            msg.arg1 = startId;
            mServiceHandler.sendMessage(msg);
        } else {
            stopSelf();
        }

        return START_STICKY;
    }

    private void checkForNotNullable(Message message, String chatKey, User currentUser, User contactUser) {

        if (receiver == null)
            throw new Error("Receiver can not be null");

        if (message == null)
            throw new Error("message can not be null");

        if (chatKey == null)
            throw new Error("chatKey can not be null");

        if (currentUser == null)
            throw new Error("currentUser can not be null");

        if (contactUser == null)
            throw new Error("contactUser can not be null");
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            onHandleMessage(msg);
        }
    }

    private void onHandleMessage(android.os.Message msg) {
        StringRequest stringRequest = (StringRequest) msg.obj;
        Volley.newRequestQueue(this).add(stringRequest);
    }

    private Map<String, Object> getMap(Chat chat, User currentUser, User contactUser, Message message) {
        String chatKey = chat.getKey();

        Map<String, Object> map = new HashMap<>();
        map.put("/"+Constants.DataBase.CHATS+"/"+contactUser.getUid()+"/"+chatKey, chat.toMap(currentUser.getUid()));

        //if (contactUser.getPhotoUrl() != null)
        chat.setPhotoUrl(contactUser.getPhotoUrl());

        chat.setDisplayName(contactUser.getDisplayName());
        chat.setReceiver(contactUser.getUid());

        map.put("/"+Constants.DataBase.CHATS+"/"+currentUser.getUid()+"/"+chatKey, chat.toMap(currentUser.getUid()));
        map.put("/"+Constants.DataBase.MESSAGES+"/"+chatKey+"/"+message.getKey(), message.toMap());

        map.put("/"+Constants.DataBase.USERS+"/"+currentUser.getUid()+"/"+Constants.DataBase.LAST_MESSAGE+"/"+chatKey, message.getKey());
        map.put("/"+Constants.DataBase.USERS+"/"+contactUser.getUid()+"/"+Constants.DataBase.LAST_MESSAGE+"/"+chatKey, message.getKey());

        map.put("/"+Constants.DataBase.USERS+"/"+currentUser.getUid()+"/"+Constants.DataBase.CHATS+"/"+contactUser.getUid(), chatKey);
        map.put("/"+Constants.DataBase.USERS+"/"+contactUser.getUid()+"/"+Constants.DataBase.CHATS+"/"+currentUser.getUid(), chatKey);

        //map.put("/"+Constants.DataBase.USERS+"/"+contactUser.getUid()+"/"+Constants.DataBase.UNREAD+"/"+message.getKey(), true);
        map.put("/"+Constants.DataBase.USERS+"/"+contactUser.getUid()+"/"+Constants.DataBase.UNREAD+"/"+chat.getKey(), true);

        return map;
    }

    private StringRequest notificationRequest(final Message message, final String chatKey, final User currentUser, final User contactUser) {

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.Net.URL_NOTIFICATION,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.Extras.MESSAGE, message);

                        if (!response.contains(Constants.SEND_NOTIFICATION_SUCCESS_RECEIVE)) {
                            receiver.send(Constants.RESULT_RECEIVER_MESSAGE_ERROR, bundle);
                            return;
                        }

                        if (!currentUser.getChats().containsKey(contactUser.getUid())) {
                            currentUser.putChat(contactUser.getUid(), chatKey);
                            currentUser.putLastMessage(chatKey, message.getKey());
                        }

                        putToFirebase(currentUser, contactUser, message, chatKey);
                        receiver.send(Constants.RESULT_RECEIVER_MESSAGE_SUCCESS, bundle);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (MessengerApplication.getInstance() == null || !MessengerApplication.getInstance().isChatIn())
                            sendNotification();

                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.Extras.MESSAGE, message);
                        receiver.send(Constants.RESULT_RECEIVER_MESSAGE_ERROR, bundle);
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        User currentUser = User.getCurrentUser(SenderService.this);

                        Map<String, String>  params = new HashMap<String, String>();

                        params.put("body", message.getMessage());
                        params.put("displayName", currentUser.getDisplayName());
                        params.put("contactUID", currentUser.getUid());
                        params.put("chatKey", chatKey);
                        params.put("lastMessageKey", message.getKey());

                        if (currentUser.getPhotoUrl() != null)
                            params.put("contactPhoto", currentUser.getPhotoUrl());

                        params.put("receiverUID", contactUser.getUid());

                        return params;
                    }
            };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        return stringRequest;
    }

    private void putToFirebase(User currentUser, User contactUser, Message message, String chatKey) {

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        String messageBody = message.getMessage();
        Chat chat = new Chat(currentUser.getDisplayName(), currentUser.getPhotoUrl(), messageBody, currentUser.getUid());
        chat.setKey(chatKey);
        chat.setLastMessageKey(message.getKey());

        Map<String, Object> map = getMap(chat, currentUser, contactUser, message);

        mDatabase.updateChildren(map);
    }

    private void sendNotification() {

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_warning)
                .setContentTitle(getString(R.string.alert_message_not_sent))
                .setContentText(getString(R.string.alert_message_not_sent_advanced))
                .setAutoCancel(true)
                .setSound( Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.sound_error));

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

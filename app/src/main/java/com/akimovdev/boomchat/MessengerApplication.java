package com.akimovdev.boomchat;

import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Looper;
import android.provider.Settings;
import android.widget.ImageView;

import com.akimovdev.boomchat.utils.FileUtils;


import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.squareup.leakcanary.LeakCanary;

public class MessengerApplication extends Application {

    private final static int CHAT_STATE_OUT = 0;
    private final static int CHAT_STATE_IN = 1;

    private volatile static MessengerApplication instance;
    private static DisplayImageOptions avatarOptions;
    private SharedPreferences sharedPreferences;


    private int chatState = CHAT_STATE_OUT;

    @Override
    public void onCreate() {
        super.onCreate();

        synchronized (MessengerApplication.class) {
            if (BuildConfig.DEBUG) {
                if (instance != null)
                    throw new RuntimeException("Something strange: there is another application newInstance.");
            }
            instance = this;
            MessengerApplication.class.notifyAll();
        }

        sharedPreferences = getSharedPreferences(Constants.APP_PREFERENCES, Context.MODE_PRIVATE);

        FileUtils.init(this);
        initImageLoader(getApplicationContext());

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);


        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);*/

    }


    public void putFCMToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.Preferences.TOKEN, token);
        editor.apply();
    }

    public String getFCMToken() {
        return sharedPreferences.getString(Constants.Preferences.TOKEN, null);
    }

    //@SuppressWarnings({"ConstantConditions", "unchecked"})
    public static MessengerApplication getInstance() {
        MessengerApplication application = instance;
        if (application == null) {
            synchronized (MessengerApplication.class) {
                if (instance == null) {
                    if (BuildConfig.DEBUG) {
                        if (Thread.currentThread() == Looper.getMainLooper().getThread())
                            throw new UnsupportedOperationException(
                                    "Current application's newInstance has not been initialized yet (wait for onCreate, please).");
                    }
                    try {
                        do {
                            MessengerApplication.class.wait();
                        } while ((application = instance) == null);
                    } catch (InterruptedException e) {
                        /* Nothing to do */
                    }
                }
            }
        }
        return application;
    }

    public static void initImageLoader(Context context) {

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        ImageLoader.getInstance().init(config.build());

        avatarOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .showImageOnFail(R.drawable.no_avatar)
                .showImageForEmptyUri(R.drawable.no_avatar)
                .showImageOnLoading(R.drawable.no_avatar)
                .imageScaleType(ImageScaleType.EXACTLY)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .build();
    }

    public void displayAvatar(String url, ImageView imageView) {
        ImageLoader.getInstance().displayImage(url, imageView, avatarOptions);
    }

    public static String getAndroidID(ContentResolver contentResolver) {

        return Settings.Secure.getString(contentResolver,
                Settings.Secure.ANDROID_ID);
    }

    public boolean isChatIn() {
        return chatState == CHAT_STATE_IN;
    }

    public void chatIn() {
        chatState = CHAT_STATE_IN;
    }

    public void chatOut() {
        chatState = CHAT_STATE_OUT;
    }

}

package com.akimovdev.boomchat;



public final class Constants {

    public static String FIREBASE_STORAGE = "gs://messenger-f1ed8.appspot.com";
    public static String URL_SERVER = "http://a0212325.xsph.ru/boomchat/";

    public static final int NOTIFICATION_CHAT_ID = 0;

    public static final int FRIENDS_START_LIST = 0;
    public static final int FRIENDS_START_NEW_CHAT = 1;

    public static final int REQUEST_CODE_CAPTURE_PHOTO = 1;
    public static final int REQUEST_CODE_CHOOSE_FROM_GALLERY = 2;
    public static final int REQUEST_CODE_AUTH_GOOGLE = 3;
    public static final int REQUEST_OPEN_PROFILE = 4;
    public static final int REQUEST_CODE_FEEDBACK = 5;

    public static final int RESULT_RECEIVER_AVATAR_CREATE = 997;
    public static final int RESULT_RECEIVER_MESSAGE_SUCCESS = 996;
    public static final int RESULT_RECEIVER_MESSAGE_ERROR = 995;

    public static final int FEEDBACK_RESULT_SUCCESS = 1;
    public static final int FEEDBACK_RESULT_FAIL = 0;

    public static final String BROADCAST_CHANGE_IN_PROFILE = "com.akimovdev.messenger.CHANGE_IN_PROFILE";

    public static final String SEND_NOTIFICATION_SUCCESS_RECEIVE = "Success";

    public static final int OPERATION_NOTHING = 0;
    public static final int OPERATION_ADD_TO_FAVORITE = 1;
    public static final int OPERATION_REMOVE_FAVORITE = -1;

    public static final long WAIT_SERVER_RESPONSE = 8000l;
    public static final String APP_PREFERENCES = "app_preferences";


    public static class DataStorage {
        private static final String STORAGE_IMAGE_DEBUG = "debug/images";
        private static final String STORAGE_IMAGE_PRODUCTION = "images";

        public static final String STORAGE_IMAGE = STORAGE_IMAGE_PRODUCTION;
    }

    public static class DataBase {
        public static final String CHATS_MAIN = "chats";
        public static final String CHATS_DEBUG = "chatsd";

        public static final String UID = "uid";
        public static final String EMAIL = "email";
        public static final String USERS = "users";
        public static final String SEARCH = "search";
        public static final String SENDER = "sender";
        public static final String STATE = "state";
        public static final String FEEDBACK = "feedback";
        public static final String TIMESTAMP = "timestamp";
        public static final String CHATS = CHATS_MAIN;
        public static final String FRIENDS = "friends";
        public static final String COUNT_FRIENDS = "countFriends";
        public static final String MEMBERS = "members";
        public static final String MESSAGE = "message";
        public static final String MESSAGES = "messages";
        public static final String UNREAD = "unread";
        public static final String LAST_MESSAGE = "lastMessage";
        public static final String LAST_SENDER = "lastSender";
        public static final String LAST_MESSAGE_KEY = "lastMessageKey";
        public static final String PHOTO_URL = "photoUrl";
        public static final String RECEIVER = "receiver";

        public static final String DISPLAY_NAME = "displayName";
        public static final String PASSWORD = "password";
        public static final String IS_DIS = "dis";
        public static final String IS_DIS_M = "disM";
    }

    public static class Extras {
        public static final String MAIN_MENU_ITEM_ID = "main_menu_item_id";
        public static final String MEMBERS = "members";
        public static final String CONTACT_USER = "contact_user";
        public static final String USER = "user";
        public static final String RESULT_RECEIVER = "result_receiver";
        public static final String IMAGE_PATH = "image_path";
        public static final String USER_UID = "user_uid";
        public static final String IS_FAVORITE = "is_favorite";
        public static final String OPERATION_RESULT = "action_result";
        public static final String FEEDBACK_RESULT = "feedback_result";
        public static final String POSITION = "position";
        public static final String CHAT_KEY = "chat_key";
        public static final String CHAT = "chat";
        public static final String MESSAGE = "message_uid";
        public static final String FRIENDS_START_MODE = "friends_start_mode";
    }

    public static class Net {
        public static final String URL_NOTIFICATION = URL_SERVER + "sendnitification.php";

    }

    public static class MainMenu {
        public static final int ITEM_MESSAGES = 0;
        public static final int ITEM_FRIENDS = 1;
        public static final int ITEM_SHARE = 2;
        public static final int ITEM_REQUEST = 3;
        public static final int ITEM_SETTINGS = 4;
        public static final int ITEM_EXIT = 5;
    }

    public static class Preferences {
        public static final String TOKEN = "token";
    }
}

package com.akimovdev.boomchat;

import android.text.TextUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidationUtils {
	
	public static boolean checkEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        if (email.length() < 1 && email.length() >  256) {
            return false;
        }
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return !matcher.matches();
    }


	public static boolean checkPassword(String password) {
		
		if (TextUtils.isEmpty(password) || password.length() < 1 || password.length() >  30) {
			return false;
		}
		
		return true;
	}

	public static boolean checkNonEmptyField(String username) {

        return !TextUtils.isEmpty(username) && !TextUtils.isEmpty(username.trim());

    }
	
    public static boolean checkPhoneNumber(String phone) {
        final String PHONE_PATTERN = "^[+]?[0-9\\s]{10,13}$";

        if (phone.length() < 10 && phone.length() >  13) {
            return false;
        }
        Pattern pattern = Pattern.compile(PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone);

        return matcher.matches();
    }

    /*public static boolean checkPhoneNumber(Phonenumber.PhoneNumber phone) {
        return PhoneNumberUtil.getInstance().isValidNumber(phone);
    }

    public static boolean checkPhoneNumber(String phone, boolean replaceNonDigits){
        final String phoneNumberText = replaceNonDigits ? phone.replace("\\D+","") : phone;
        PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
        Phonenumber.PhoneNumber phoneNumber;
        try {
            phoneNumber = phoneNumberUtil.parse("+" + phoneNumberText,null);
            if(!phoneNumberUtil.isPossibleNumber(phoneNumber))
                throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER,"impossible number");
        } catch (NumberParseException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    } */

}

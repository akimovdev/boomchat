package com.akimovdev.boomchat.model;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.akimovdev.boomchat.Constants;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;
import java.util.Map;

public class User implements Parcelable {

    private String messageNotCurrentUser = "This exapmle is not current user";

    private String uid;
    private String displayName;
    private String email;
    private String password;
    //protected String photoUrl = "https://firebasestorage.googleapis.com/v0/b/messenger-f1ed8.appspot.com/o/images%2Fno_avatar.png?alt=media&token=b20f338e-fd25-4e12-bc29-86363e4ca19d";
    protected String photoUrl = "https://firebasestorage.googleapis.com/v0/b/boomchat-fcb79.appspot.com/o/system%2Fcap.png?alt=media&token=ff032952-f660-4935-8cc1-e2b176e69ad1";

    protected Map<String, Boolean> friends;
    protected Map<String, String> chats;
    protected Map<String, String> lastMessages;

    public User() {
        friends = new HashMap<>();
        chats = new HashMap<>();
        lastMessages = new HashMap<>();
    }

    protected User(Parcel in) {
        messageNotCurrentUser = in.readString();
        uid = in.readString();
        displayName = in.readString();
        email = in.readString();
        password = in.readString();
        photoUrl = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public static final User createCurrentUser(Context context, FirebaseUser firebaseUser, String email, String displayName, String photoUrl, Map<String, Boolean> friends, Map<String, String> chats, Map<String, String> lastMessages, String password, int unread) {
        return CurrentUser.createInstance(context, firebaseUser, email, displayName, photoUrl, friends, chats, lastMessages, password, unread);
    }

    public static User getCurrentUser(Context context) {
        return CurrentUser.getInstance(context);
    }

    private User getCurrentUser() {
        return CurrentUser.getInstance();
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUid() {
        return uid;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getSymbolAvatar() {
        return getSymbolAvatar(getDisplayName());
        //return getDisplayName().substring(0,1).toUpperCase();
    }

    public static String getSymbolAvatar(String displayName) {
        return displayName.substring(0,1).toUpperCase();
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhotoUri(Uri photoUri) {
        // DON'T REMOVE
        //this.photoUrl = photoUri.toString();
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public int getCountFriends() {
        return friends.size();
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void logout() {
        throw new Error(messageNotCurrentUser);
    }

    public void clearCurrentUser() {
        throw new Error(messageNotCurrentUser);
    }

    public boolean isCurrentUser() {
        return this.uid.equals(getCurrentUser().getUid());
    }

    public void saveToServer() {
        saveToServer(null);
    }

    public void putFriends(Map<String, Boolean> friends) {
        this.friends.putAll(friends);
    }

    public void putChats(Map<String, String> chats) {
        this.chats.putAll(chats);
    }

    public void putFriend(String uid) {
        friends.put(uid, true);
    }

    public void removeFriend(String uid) {
        friends.remove(uid);
    }

    public void putChat(String uid, String chatKey) {
        chats.put(uid, chatKey);
    }

    public void putLastMessage(String key, String messageKey) {
        this.lastMessages.put(key, messageKey);
    }

    public void putLastMessages(Map<String, String> lastMessages) {
        this.lastMessages.putAll(lastMessages);
    }

    public Map<String, String> getChats() {
        return chats;
    }

    public Map<String, String> getLastMessages() {
        return lastMessages;
    }

    public boolean isFriend(String uid) {
        return friends.containsKey(uid);
    }

    public void saveToServer(DatabaseReference.CompletionListener completionListener) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS).child(getUid());
        mDatabase.setValue(this, completionListener);
    }

    @Exclude
    public void setUnread(int unread) {
        throw new Error("Available only for current user");
    }

    @Exclude
    public int getUnread() {
        throw new Error("Available only for current user");
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Map<String, Boolean> getSearchMetadata() {

        String displayName = this.displayName.toLowerCase();

        Map<String, Boolean> map = new HashMap<>();

        for (int i=1; i<=displayName.length(); i++) {
            map.put(displayName.substring(0,i), true);
        }

        return map;
    }

    public Map<String, Object> toMap() {

        Map<String, Object> map = new HashMap<>();

        map.put(Constants.DataBase.EMAIL, email);
        map.put(Constants.DataBase.DISPLAY_NAME, displayName);
        map.put(Constants.DataBase.PHOTO_URL, getPhotoUrl());
        map.put(Constants.DataBase.UID, getUid());

        return map;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(messageNotCurrentUser);
        dest.writeString(uid);
        dest.writeString(displayName);
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(photoUrl);
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof User)
            return ((User) obj).getUid().equals(getUid());

        return false;
    }
}

package com.akimovdev.boomchat.model.request.firebase;


import com.google.android.gms.tasks.OnSuccessListener;

public abstract class OnSuccessTimeUpListener implements OnSuccessListener<Void>, OnTimeUpListener {

    private FirebaseTimeResponseRequest firebaseTimeResponse = null;

    @Override
    public void onSuccess(Void aVoid) {
        if (firebaseTimeResponse == null)
            throw new Error("Your must call buildListener() method before set listener");

        firebaseTimeResponse.stopWaitResponse();
    }

    public abstract void onTimeUpResponse();

    OnSuccessTimeUpListener buildListener(FirebaseTimeResponseRequest firebaseTimeResponse) {
        this.firebaseTimeResponse = firebaseTimeResponse;
        return this;
    }
}

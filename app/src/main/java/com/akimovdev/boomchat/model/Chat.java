package com.akimovdev.boomchat.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import com.akimovdev.boomchat.Constants;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

public class Chat implements Parcelable, Comparable {

    private String lastMessage;
    private String lastMessageKey;
    private String lastSender;
    private String photoUrl;
    private String displayName;
    private String receiver;
    private long timestamp;
    private String key;
    private boolean unread = false;

    public Chat() {
        // Required empty public constructor
    }

    public Chat(String displayName, String photoUrl, String lastMessage, String receiver) {
        this.displayName = displayName;
        this.photoUrl = photoUrl;
        this.lastMessage = lastMessage;
        this.receiver = receiver;
    }

    protected Chat(Parcel in) {
        lastMessage = in.readString();
        lastMessageKey = in.readString();
        lastSender = in.readString();
        photoUrl = in.readString();
        displayName = in.readString();
        receiver = in.readString();
        timestamp = in.readLong();
        key = in.readString();
        unread = in.readByte() != 0;
    }

    public static final Creator<Chat> CREATOR = new Creator<Chat>() {
        @Override
        public Chat createFromParcel(Parcel in) {
            return new Chat(in);
        }

        @Override
        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    };

    public void setLastMessageKey(String lastMessageKey) {
        this.lastMessageKey = lastMessageKey;
    }

    public String getLastMessageKey() {
        return lastMessageKey;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastSender(String lastSender) {
        this.lastSender = lastSender;
    }

    public String getLastSender() {
        return lastSender;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getReceiver() {
        return receiver;
    }

    public Map<String, Object> toMap(String currentUID) {

        Map<String,Object> map = new HashMap<>();
        map.put(Constants.DataBase.LAST_MESSAGE, lastMessage);
        map.put(Constants.DataBase.LAST_MESSAGE_KEY, lastMessageKey);
        map.put(Constants.DataBase.LAST_SENDER, currentUID);
        map.put(Constants.DataBase.DISPLAY_NAME, displayName);
        map.put(Constants.DataBase.PHOTO_URL, photoUrl);
        map.put(Constants.DataBase.RECEIVER, receiver);
        map.put(Constants.DataBase.UNREAD, true);
        map.put(Constants.DataBase.TIMESTAMP, ServerValue.TIMESTAMP);

        return map;
    }

    public void setUnread(boolean unread) {
        this.unread = unread;
    }

    public boolean isUnread() {
        return unread;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Chat)
            return ((Chat) obj).getKey().equals(getKey());

        return false;
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        byte unreadByte = (byte) (unread ? 1 : 0);

        dest.writeString(lastMessage);
        dest.writeString(lastMessageKey);
        dest.writeString(lastSender);
        dest.writeString(photoUrl);
        dest.writeString(displayName);
        dest.writeString(receiver);
        dest.writeLong(timestamp);
        dest.writeString(key);
        dest.writeByte(unreadByte);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Chat chat = (Chat) o;

        if (timestamp > chat.getTimestamp()) {
            return 1;
        } else if (timestamp < chat.getTimestamp()) {
            return -1;
        }

        return 0;
    }
}

package com.akimovdev.boomchat.model.request.firebase;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;

public abstract class ValueTimeUpListener implements ValueEventListener, OnTimeUpListener {

    private FirebaseTimeResponseRequest firebaseTimeResponse = null;

    public abstract void onTimeUpResponse();

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (firebaseTimeResponse == null)
            throw new Error("Your must call buildListener() method before set listener");

        firebaseTimeResponse.stopWaitResponse();
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        /* Nothing to do */
    }

    ValueTimeUpListener buildListener(FirebaseTimeResponseRequest firebaseTimeResponse) {
        this.firebaseTimeResponse = firebaseTimeResponse;
        return this;
    }
}

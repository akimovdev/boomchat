package com.akimovdev.boomchat.model.request.firebase;

import android.app.Activity;
import android.os.Handler;

import com.google.firebase.database.DatabaseReference;

import java.util.Map;

public class FirebaseTimeResponseRequest {

    private final static long DEFAULT_TIME_WAIT_RESPONSE = 8000l;

    private Handler waitResponseHandler;
    private Runnable waitRunnable;
    private OnTimeUpListener listener;

    private DatabaseReference databaseReference = null;

    private FirebaseTimeResponseRequest() { /* Nothing to do */ }

    public static FirebaseTimeResponseRequest createRequest(DatabaseReference databaseReference) {
        FirebaseTimeResponseRequest request = new FirebaseTimeResponseRequest();
        request.databaseReference = databaseReference;

        return request;
    }

    public void setTimeUpValueListener(ValueTimeUpListener listener) {
        this.listener = listener;
        databaseReference.addValueEventListener(listener.buildListener(this));
        startWaitResponse();
    }

    public void setValue(Object o, Activity activity, OnSuccessTimeUpListener listener)  {
        this.listener = listener;
        databaseReference.setValue(o).addOnSuccessListener(activity, listener.buildListener(this));

        startWaitResponse();
    }

    public void updateChildren(Map<String, Object> map, Activity activity, OnSuccessTimeUpListener listener) {
        this.listener = listener;

        databaseReference.updateChildren(map).addOnSuccessListener(activity, listener.buildListener(this));


        startWaitResponse();
    }

    private void startWaitResponse() {
        waitResponseHandler = new Handler();
        waitRunnable = new Runnable() {
            @Override
            public void run() {
                listener.onTimeUpResponse();
                listener = null;
            }
        };

        waitResponseHandler.postDelayed(waitRunnable, DEFAULT_TIME_WAIT_RESPONSE);
    }

    public void stopWaitResponse() {
        if (waitResponseHandler != null)
            waitResponseHandler.removeCallbacks(waitRunnable);
    }
}

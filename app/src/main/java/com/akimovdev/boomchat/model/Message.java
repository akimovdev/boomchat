package com.akimovdev.boomchat.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import com.akimovdev.boomchat.Constants;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;
import java.util.HashMap;
import java.util.Map;

public class Message implements Parcelable, Comparable {

    public final static int STATE_NORMAL = 0;
    public final static int STATE_FAIL = 1;
    public final static int STATE_PROGRESS = 2;
    public final static int STATE_UNREAD = 3;

    private String key;
    private String sender;
    private String message;
    private long timestamp;

    private int state = STATE_NORMAL;

    public Message() {}

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSender() {
        return sender;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Exclude
    public boolean isState(int state) {
        return this.state == state;
    }

    public Map <String, Object> toMap() {
        Map <String, Object> map = new HashMap<>();
        map.put(Constants.DataBase.MESSAGE, message);
        map.put(Constants.DataBase.SENDER, sender);
        map.put(Constants.DataBase.TIMESTAMP, ServerValue.TIMESTAMP);
        map.put(Constants.DataBase.STATE, STATE_UNREAD);

        return map;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Message) {
            //return ((Message) obj).getTimestamp() == this.getTimestamp();
            return ((Message) obj).getKey().equals(getKey());
        }

        return false;
    }

    protected Message(Parcel in) {
        key = in.readString();
        sender = in.readString();
        message = in.readString();
        timestamp = in.readLong();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel in) {
            return new Message(in);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(key);
        dest.writeString(sender);
        dest.writeString(message);
        dest.writeLong(timestamp);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        Message m = (Message) o;
        return getKey().compareTo(m.getKey());
    }
}

package com.akimovdev.boomchat.model;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.akimovdev.boomchat.Constants;
import com.google.android.gms.auth.api.Auth;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;

class CurrentUser extends User {

    private final static String CURRENT_USER_PREFERENCES = "current_user_preferences";

    private final static String FIELD_PASSWORD = "field_password";
    private final static String FIELD_DISPLAY_NAME = "field_display_name";
    private final static String FIELD_PHOTO_URL = "field_photo_url";
    private final static String FIELD_FRIENDS = "field_friends";
    private final static String FIELD_CHATS = "field_chats";
    private final static String FIELD_LAST_MESSAGES = "field_last_messages";
    private final static String FIELD_UNREAD = "field_unread";

    private static CurrentUser instance;
    private FirebaseUser firebaseUser;
    private static SharedPreferences sharedPreferences;

    @Exclude
    private int unread = 0;

    static CurrentUser getInstance(Context context) {

        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if (firebaseUser != null) {
            if (instance == null)
                instance = new CurrentUser(context);
        } else {
            instance = null;
        }

        return instance;
    }

    static CurrentUser getInstance() {
        return instance;
    }

    public static CurrentUser createInstance(Context context, FirebaseUser firebaseUser, String email, String displayName, String photoUrl, Map<String, Boolean> friends, Map<String, String> chats, Map<String, String> lastMessages, String password, int unread) {

        sharedPreferences = context.getSharedPreferences(CURRENT_USER_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        CurrentUser user = new CurrentUser();
        user.setEmail(email);
        user.setDisplayName(displayName);
        user.setPassword(password);
        //user.photoUrl = photoUrl;

        if (friends != null)
            user.friends.putAll(friends);

        if (chats != null)
            user.chats.putAll(chats);

        if (lastMessages != null)
            user.lastMessages.putAll(lastMessages);

        Gson gson = new GsonBuilder().create();

        editor.putString(FIELD_DISPLAY_NAME, displayName);
        //editor.putString(FIELD_PHOTO_URL, photoUrl);
        editor.putString(FIELD_PASSWORD, password);

        if (friends != null && !friends.isEmpty())
            editor.putString(FIELD_FRIENDS, gson.toJson(friends));

        if (chats != null && !chats.isEmpty())
            editor.putString(FIELD_CHATS, gson.toJson(chats));

        if (lastMessages != null && !lastMessages.isEmpty())
            editor.putString(FIELD_LAST_MESSAGES, gson.toJson(lastMessages));

        editor.putInt(FIELD_UNREAD, unread);

        editor.apply();

        instance = user;

        return user;
    }

    private CurrentUser(){
        super();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    private CurrentUser(Context context) {
        super();

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        sharedPreferences = context.getSharedPreferences(CURRENT_USER_PREFERENCES, Context.MODE_PRIVATE);

        String photoUrl = sharedPreferences.getString(FIELD_PHOTO_URL, null);

        super.setPassword(sharedPreferences.getString(FIELD_PASSWORD, null));
        super.setDisplayName(sharedPreferences.getString(FIELD_DISPLAY_NAME, null));
        setUnread(sharedPreferences.getInt(FIELD_UNREAD, 0));

        String friendsJson = sharedPreferences.getString(FIELD_FRIENDS, null);
        String chatsJson = sharedPreferences.getString(FIELD_CHATS, null);
        String lastMessagesJson = sharedPreferences.getString(FIELD_LAST_MESSAGES, null);

        if (friendsJson != null)
            putFriends(friendsJson);

        if (chatsJson != null)
            putChats(chatsJson);

        if (lastMessagesJson != null)
            putLastMessages(lastMessagesJson);

        if (photoUrl != null)
            super.setPhotoUri(Uri.parse(photoUrl));

        FirebaseDatabase.getInstance()
                .getReference(Constants.DataBase.USERS)
                .child(getUid())
                .keepSynced(true);
    }

    @Override
    public void setPhotoUri(Uri photoUri) {
        // DON'T REMOVE
        /*SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FIELD_PHOTO_URL, photoUri.toString());
        editor.apply();

        super.setPhotoUri(photoUri); */
    }

    @Override
    public String getUid() {
        return firebaseUser.getUid();
    }

    @Override
    public String getEmail() {

        if (firebaseUser.getEmail() != null)
            return firebaseUser.getEmail();
        else
            return super.getEmail();
    }

    @Override
    public boolean isCurrentUser() {
        return true;
    }

    public void putChats(String json) {

        if (json == null || json.isEmpty() || json.equals("null"))
            return;

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Gson gson = new GsonBuilder().create();
        Map<String, String> chatsMap = gson.fromJson(json, type);
        super.putChats(chatsMap);
    }

    private void putFriends(String json) {

        if (json == null || json.isEmpty() || json.equals("null"))
            return;

        Type type = new TypeToken<Map<String, Boolean>>(){}.getType();
        Gson gson = new GsonBuilder().create();
        Map<String, Boolean> friendsMap = gson.fromJson(json, type);
        super.putFriends(friendsMap);
    }


    public void putLastMessages(String json) {

        if (json == null || json.isEmpty() || json.equals("null"))
            return;

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Gson gson = new GsonBuilder().create();
        Map<String, String> lastMessagesMap = gson.fromJson(json, type);
        super.putLastMessages(lastMessagesMap);
    }

    @Override
    public void putLastMessage(String key, String messageKey) {
        super.putLastMessage(key, messageKey);
        writeLastMessageToCash();
    }

    @Override
    public void putChat(String uid, String chatKey) {
        super.putChat(uid, chatKey);
        writeChatsToCash();
    }

    @Override
    public void putFriend(String uid) {
        super.putFriend(uid);
        writeFriendsToCash();
    }

    @Override
    public void removeFriend(String uid) {
        super.removeFriend(uid);
        writeFriendsToCash();
    }

    private void writeLastMessageToCash() {
        Gson gson = new GsonBuilder().create();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        if ( super.lastMessages == null || super.lastMessages.isEmpty() )
            editor.putString(FIELD_LAST_MESSAGES, "");
        else
            editor.putString(FIELD_LAST_MESSAGES, gson.toJson(super.lastMessages));

        editor.apply();
    }


    private void writeChatsToCash() {
        Gson gson = new GsonBuilder().create();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        if ( super.chats == null || super.chats.isEmpty() )
            editor.putString(FIELD_CHATS, "");
        else
            editor.putString(FIELD_CHATS, gson.toJson(super.chats));

        editor.apply();
    }

    private void writeFriendsToCash() {
        Gson gson = new GsonBuilder().create();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        if ( super.friends == null || super.friends.isEmpty() )
            editor.putString(FIELD_FRIENDS, "");
        else
            editor.putString(FIELD_FRIENDS, gson.toJson(super.friends));

        editor.apply();
    }

    @Override
    public void setDisplayName(String displayName) {

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(FIELD_DISPLAY_NAME, displayName);
        editor.apply();

        super.setDisplayName(displayName);
    }

    @Exclude
    @Override
    public void setUnread(int unread) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(FIELD_UNREAD, unread);
        editor.apply();

        this.unread = unread;
    }

    @Exclude
    @Override
    public int getUnread() {
        return unread;
    }

    @Override
    public void logout() {
        //Auth.GoogleSignInApi.signOut(mGoogleApiClient)
        FirebaseAuth.getInstance().signOut();
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }
}

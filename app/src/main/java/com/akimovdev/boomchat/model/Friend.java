package com.akimovdev.boomchat.model;


import java.util.HashMap;
import java.util.Map;

public class Friend {

    public static final int STATE_APPROVED = 0;
    public static final int STATE_NO_RELATIONS = 1;

    public static final String ERR_KEY_CANNOT_BE_NULL = "key cannot be null";

    private String key;
    private String displayName;
    private String photoUrl;
    private int state;

    public static Friend createInstance(User user) {

        Friend friend = new Friend();
        friend.setKey(user.getUid());
        friend.setDisplayName(user.getDisplayName());

        if (user.getPhotoUrl() != null)
            friend.setPhotoUrl(user.getPhotoUrl());

        return friend;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        if (key == null)
            throw new Error(ERR_KEY_CANNOT_BE_NULL);
        return key;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getState() {
        return state;
    }

    public Map<String, Object> toMap(int state) {
        Map<String, Object> map = new HashMap<>();
        map.put("photoUrl", photoUrl);
        map.put("displayName", displayName);
        map.put("state", state);

        return map;
    }
}

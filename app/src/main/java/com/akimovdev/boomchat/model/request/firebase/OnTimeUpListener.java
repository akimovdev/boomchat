package com.akimovdev.boomchat.model.request.firebase;

public interface OnTimeUpListener {
    void onTimeUpResponse();
}

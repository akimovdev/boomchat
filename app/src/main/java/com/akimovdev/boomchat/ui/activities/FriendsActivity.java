package com.akimovdev.boomchat.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.common.view.SlidingTabLayout;
import com.akimovdev.boomchat.ui.fragments.FriendListFragment;

public class FriendsActivity extends AppCompatActivity {

    private SlidingTabLayout mSlidingTabLayout;
    private ViewPager mViewPager;
    private int startMode;
    private static final int[] TAB_ITEMS = new int[] { R.string.tab_favorite,
                                                       R.string.tab_all };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_shadow);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.title_screen_contacts));
        }

        if (savedInstanceState != null)
            return;

        startMode = getIntent().getIntExtra(Constants.Extras.FRIENDS_START_MODE, Constants.FRIENDS_START_LIST);

        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(getSupportFragmentManager()));
        mSlidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setCustomTabView(R.layout.tabs_text_bg, R.id.tabsText);
        mSlidingTabLayout.setViewPager(mViewPager);

        mSlidingTabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(FriendsActivity.this, R.color.colorIconsText);
            }

            @Override
            public int getDividerColor(int position) {
                return 0;
            }
        });

        registerReceiver(brd_receiver, new IntentFilter(Constants.BROADCAST_CHANGE_IN_PROFILE));
    }

    private class SamplePagerAdapter extends FragmentStatePagerAdapter {

        SparseArray<Fragment> registeredFragments;

        SamplePagerAdapter(FragmentManager fm) {
            super(fm);
            registeredFragments = new SparseArray<>();
        }

        @Override
        public int getCount() {
            return TAB_ITEMS.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString( TAB_ITEMS[position] );
        }

        @Override
        public Fragment getItem(int position) {

            if (position == 0)
                return FriendListFragment.newInstanceFavorite(startMode);

            return FriendListFragment.newInstanceAll(startMode);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisterFragment(int position) {
            return registeredFragments.get(position);
        }
    }

    private void onChangesInProfile(Intent intent) {

        User user = intent.getExtras().getParcelable(Constants.Extras.USER);
        int operation = intent.getExtras().getInt(Constants.Extras.OPERATION_RESULT);
        int position = intent.getExtras().getInt(Constants.Extras.POSITION);

        SamplePagerAdapter adapter = (SamplePagerAdapter) mViewPager.getAdapter();
        int countItems = adapter.getCount();

        for (int i=0; i<countItems; i++) {

            Fragment fragment = adapter.getRegisterFragment(i);
            if (fragment != null && fragment instanceof FriendListFragment) {
                ( (FriendListFragment) fragment ).onChangesInProfile(user, position, operation);
            }
        }
    }

    BroadcastReceiver brd_receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Constants.BROADCAST_CHANGE_IN_PROFILE)) {
                onChangesInProfile(intent);
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(brd_receiver);
    }

    public static void startFriendsActivity(Context context) {
        Intent intent = new Intent(context, FriendsActivity.class);
        intent.putExtra(Constants.Extras.FRIENDS_START_MODE, Constants.FRIENDS_START_LIST);
        context.startActivity(intent);
    }

    public static void startChatFriendsActivity(Context context) {
        Intent intent = new Intent(context, FriendsActivity.class);
        intent.putExtra(Constants.Extras.FRIENDS_START_MODE, Constants.FRIENDS_START_NEW_CHAT);
        context.startActivity(intent);
    }
}

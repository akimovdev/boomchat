package com.akimovdev.boomchat.ui.fragments;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.activities.MessengerActivity;
import com.akimovdev.boomchat.ui.activities.ProfileActivity;
import com.akimovdev.boomchat.ui.adapters.FriendsAdapter;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;

import static android.app.Activity.RESULT_OK;

public class FriendListFragment extends Fragment {

    private static final String ARG_MODE = "mode";


    private static final int MODE_FAVORITE = 0;
    private static final int MODE_ALL = 1;

    private SearchView searchView;
    private TextView tvEmptyList;
    private RecyclerView rvFriends;
    private ProgressBar progressBar;
    private boolean isSearched = false;
    private int mode = MODE_ALL;
    private int startMode = Constants.FRIENDS_START_LIST;
    private OnListFragmentInteractionListener mListener;

    public FriendListFragment() {}

    public static FriendListFragment newInstanceFavorite(int startMode) {
        FriendListFragment fragment = new FriendListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, MODE_FAVORITE);
        args.putInt(Constants.Extras.FRIENDS_START_MODE, startMode);
        fragment.setArguments(args);
        return fragment;
    }

    public static FriendListFragment newInstanceAll(int startMode) {
        FriendListFragment fragment = new FriendListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_MODE, MODE_ALL);
        args.putInt(Constants.Extras.FRIENDS_START_MODE, startMode);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mode = getArguments().getInt(ARG_MODE, MODE_ALL);
            startMode = getArguments().getInt(Constants.Extras.FRIENDS_START_MODE, Constants.FRIENDS_START_LIST);
        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_friend_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final Context context = view.getContext();
        rvFriends = (RecyclerView) view.findViewById(R.id.rvFriends);
        tvEmptyList = (TextView) view.findViewById(R.id.tvEmptyList);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        mListener = getListener(context);

        rvFriends.setLayoutManager(new LinearLayoutManager(context));

        FriendsAdapter adapter = new FriendsAdapter(view.getContext(), mListener);

        if (mode == MODE_FAVORITE)
            adapter.sortFavorite();
        else
            adapter.sortAll();

        rvFriends.setAdapter(adapter);

        User currentUser = User.getCurrentUser(context);
        if (currentUser.getCountFriends() == 0) {
            tvEmptyList.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        final FriendsAdapter friendsAdapter = (FriendsAdapter) rvFriends.getAdapter();
        friendsAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                super.onItemRangeRemoved(positionStart, itemCount);

                if (friendsAdapter.getItemCount() == 0)
                    tvEmptyList.setVisibility(View.VISIBLE);
            }
        });


        if (!isNetworkConnected()) {
            tvEmptyList.setText(getString(R.string.no_internet_connection));

            if (friendsAdapter.getItemCount() == 0)
                tvEmptyList.setVisibility(View.VISIBLE);

            progressBar.setVisibility(View.GONE);
        } else {
            tvEmptyList.setText(getString(R.string.empty));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        FriendsAdapter  adapter = (FriendsAdapter) rvFriends.getAdapter();
        adapter.cleanupListener();
    }

    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void openProfile(User item, int adapterPosition) {
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra(Constants.Extras.USER_UID, item.getUid());
        intent.putExtra(Constants.Extras.USER, item);

        intent.putExtra(Constants.Extras.POSITION, adapterPosition);
        startActivityForResult(intent, Constants.REQUEST_OPEN_PROFILE);
    }

    private void onOpenChat(User user) {

        User currentUser = User.getCurrentUser(getContext());

        Chat chat = null;

        if (currentUser.getChats().containsKey(user.getUid())) {
            chat = new Chat();
            String chatKey = currentUser.getChats().get(user.getUid());

            chat.setKey(chatKey);
            chat.setLastMessageKey( currentUser.getLastMessages().get(chatKey) );
        }

        MessengerActivity.startNewChatActivity(getContext(), user, chat);
    }

    private OnListFragmentInteractionListener getListener(final Context context) {
        return new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(User item, int position, int adapterPosition) {

                if (startMode == Constants.FRIENDS_START_NEW_CHAT) {
                    onOpenChat(item);
                    getHostActivity().finish();
                } else {
                    openProfile(item, adapterPosition);
                }

            }

            @Override
            public void onListFragmentCount(long count) {
                if (count == 0) {
                    tvEmptyList.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadComplete(int size) {
                if (rvFriends.getAdapter().getItemCount() == 0) {
                    tvEmptyList.setVisibility(View.VISIBLE);
                } else {
                    tvEmptyList.setVisibility(View.GONE);
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFail(String message) {
                Toast.makeText(MessengerApplication.getInstance().getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item_menu, menu);

        // *** DON'T REMOVE ***/
        //initSearch(menu.findItem(R.id.action_search));

        super.onCreateOptionsMenu(menu,inflater);
    }

    private void initSearch(MenuItem searchItem) {

        SearchManager searchManager = (SearchManager) getHostActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        if (searchView != null) {

            searchView.setSearchableInfo(searchManager.getSearchableInfo(getHostActivity().getComponentName()));
            searchView.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    if (!isSearched)
                        return false;

                    /*FriendsAdapter adapter = (FriendsAdapter) rvFriends.getAdapter();
                    User user = User.getCurrentUser(getContext());
                    adapter.clear();

                    //tabHost.setVisibility(View.VISIBLE);

                    if (user.getCountFriends() != 0)
                        tvEmptyList.setVisibility(View.GONE);
                    else
                        tvEmptyList.setVisibility(View.VISIBLE);

                    isSearched = false; */

                    return false;
                }
            });

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    isSearched = true;

                    //tabHost.setVisibility(View.GONE);
                    /*tvEmptyList.setVisibility(View.GONE);
                    progressBar.setVisibility(View.VISIBLE);

                    FriendsAdapter adapter = (FriendsAdapter) rvFriends.getAdapter();

                    if ( !getHostActivity().isNetworkConnected() ) {
                        tvEmptyList.setVisibility(View.VISIBLE);
                        tvEmptyList.setText(getString(R.string.no_internet_connection));
                        adapter.searchFriends("");
                        return false;
                    } else {
                        tvEmptyList.setText(getString(R.string.empty));
                        adapter.searchFriends(newText.toLowerCase());
                    } */

                    return false;
                }
            });
        }
    }

    private void addToFavorite(User user, int position) {
        FriendsAdapter adapter = (FriendsAdapter) rvFriends.getAdapter();
        adapter.addToFavorite(user, position);

        tvEmptyList.setVisibility(View.GONE);
    }

    private void removeFavorite(User user) {
        FriendsAdapter adapter = (FriendsAdapter) rvFriends.getAdapter();
        adapter.removeFromFavorite(user);
    }

    public void onChangesInProfile(User user, int position, int operation) {

        if (operation == Constants.OPERATION_ADD_TO_FAVORITE)
            addToFavorite(user, position);
        else if (operation == Constants.OPERATION_REMOVE_FAVORITE)
            removeFavorite(user);
    }

    private Activity getHostActivity() {
        return getActivity();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_OPEN_PROFILE) {
            if (resultCode == RESULT_OK) {
                //handleOpenProfileResult(data);
            }
        }
    }

    /*@Override
    public boolean onBackPressed() {

        if (searchView == null)
            return true;

        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return false;
        }

        return true;
    } */

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(User item, int position, int adapterPosition);
        void onListFragmentCount(long count);
        void onLoadComplete(int size);
        void onFail(String message);
    }
}

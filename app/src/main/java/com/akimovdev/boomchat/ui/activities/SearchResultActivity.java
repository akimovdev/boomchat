package com.akimovdev.boomchat.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.akimovdev.boomchat.R;

public class SearchResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
    }
}

package com.akimovdev.boomchat.ui.fragments;


import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akimovdev.boomchat.R;

public class AboutFragment extends BaseFragment {

    private TextView tvVersion;

    public AboutFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_about, container, false);

        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(getString(R.string.title_screen_about));
        tvVersion = (TextView) view.findViewById(R.id.tvVersion);

        try {
            PackageInfo pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
            tvVersion.setText("Version "+pInfo.versionName+"\n"+"Build "+pInfo.versionCode+" from 18 apr 2017");

        } catch (PackageManager.NameNotFoundException ex) {
            ex.printStackTrace();
        }

        return view;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}

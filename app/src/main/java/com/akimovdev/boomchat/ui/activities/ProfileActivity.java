package com.akimovdev.boomchat.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.akimovdev.boomchat.model.Friend;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.model.request.firebase.FirebaseTimeResponseRequest;
import com.akimovdev.boomchat.model.request.firebase.OnSuccessTimeUpListener;
import com.akimovdev.boomchat.ui.fragments.ProfileFragment;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends BaseActivity {

    private User user;
    private int position = -1;
    private boolean startIsFriend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_overlay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (savedInstanceState != null)
            return;

        user = getIntent().getExtras().getParcelable(Constants.Extras.USER);
        position = getIntent().getExtras().getInt(Constants.Extras.POSITION, -1);

        if (user == null)
            throw new Error("User can not be null");

        String userUid = user.getUid();
        startIsFriend = User.getCurrentUser(this).isFriend(userUid);

        addFragment(ProfileFragment.newInstance());

        /*FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS).child(userUid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        user = dataSnapshot.getValue(User.class);
                        addFragment(ProfileFragment.newInstance());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Toast.makeText(ProfileActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }); */
    }

    public User getUser() {
        return user;
    }

    public void declineFriend() {

        //action = startIsFriend ? Constants.OPERATION_REMOVE_FAVORITE : Constants.OPERATION_NOTHING;

        sendBroadcast(Constants.OPERATION_REMOVE_FAVORITE);

        Friend friend = Friend.createInstance(user);

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        User currentUser = User.getCurrentUser(this);
        currentUser.removeFriend(friend.getKey());

        Map<String, Object> map = new HashMap<>();

        map.put("/"+Constants.DataBase.FRIENDS+"/"+currentUser.getUid()+"/"+friend.getKey(), null);
        map.put("/"+Constants.DataBase.USERS+"/"+currentUser.getUid()+"/"+Constants.DataBase.FRIENDS+"/"+friend.getKey(), null);

        //mDatabase.updateChildren(map);
        showProgressDialog();

        FirebaseTimeResponseRequest.createRequest(mDatabase)
                .updateChildren(map, this, new OnSuccessTimeUpListener() {

                    @Override
                    public void onSuccess(Void aVoid) {
                        super.onSuccess(aVoid);
                        hideProgressDialog();
                    }

                    @Override
                    public void onTimeUpResponse() {
                        hideProgressDialog();
                        showAlertDialog(getString(R.string.title_internet_no_available),
                                getString(R.string.alert_internet_no_available));
                    }
                });
    }

    public void addFriend() {

        sendBroadcast( Constants.OPERATION_ADD_TO_FAVORITE );

        Friend friend = Friend.createInstance(user);

        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        User currentUser = User.getCurrentUser(this);
        currentUser.putFriend(friend.getKey());
        Map<String, Object> map = new HashMap<>();

        map.put("/"+Constants.DataBase.FRIENDS+"/"+currentUser.getUid()+"/"+friend.getKey(), friend.toMap(Friend.STATE_APPROVED));
        map.put("/"+Constants.DataBase.USERS+"/"+currentUser.getUid()+"/"+Constants.DataBase.FRIENDS+"/"+friend.getKey(), true);

        showProgressDialog();

        FirebaseTimeResponseRequest.createRequest(mDatabase)
                .updateChildren(map, this, new OnSuccessTimeUpListener() {

                    @Override
                    public void onSuccess(Void aVoid) {
                        super.onSuccess(aVoid);
                        hideProgressDialog();
                    }

                    @Override
                    public void onTimeUpResponse() {
                        hideProgressDialog();
                        showAlertDialog(getString(R.string.title_internet_no_available),
                                getString(R.string.alert_internet_no_available));
                    }
                });
    }

    private void sendBroadcast(int operation) {

        Intent data = new Intent();

        data.setAction(Constants.BROADCAST_CHANGE_IN_PROFILE);
        data.putExtra(Constants.Extras.POSITION, position);
        data.putExtra(Constants.Extras.OPERATION_RESULT, operation);
        data.putExtra(Constants.Extras.USER, user);

        sendBroadcast(data);
    }

    public boolean isFavorite() {
        return User.getCurrentUser(this).isFriend(user.getUid());
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }

    @Override
    public void finishActivity(int requestCode) {
        Toast.makeText(this, "finishActivity", Toast.LENGTH_SHORT).show();
        super.finishActivity(requestCode);
    }

    public static void startProfileActivity(Context context, String uid, boolean isFavorite) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(Constants.Extras.USER_UID, uid);
        intent.putExtra(Constants.Extras.IS_FAVORITE, isFavorite);
        context.startActivity(intent);
    }
}

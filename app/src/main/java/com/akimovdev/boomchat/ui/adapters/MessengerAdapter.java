package com.akimovdev.boomchat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.Message;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.MessengerFragment;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.ContentValues.TAG;

public class MessengerAdapter extends RecyclerView.Adapter<MessengerAdapter.ViewHolder> {

    private int COUNT_ITEMS_LOAD = 60;

    private final MessengerFragment.OnListFragmentInteractionListener mListener;
    private List<Message> messageList = new ArrayList<>();
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListener;
    private Map<String, User> members;
    private User currentUser;

    private void removeUnread(String chatKey) {

        Map<String, Object> unreadMap = new HashMap<>();
        unreadMap.put("/"+Constants.DataBase.USERS+"/"+currentUser.getUid()+"/"+Constants.DataBase.UNREAD+"/"+chatKey, null);
        unreadMap.put("/"+Constants.DataBase.CHATS+"/"+currentUser.getUid()+"/"+chatKey+"/"+Constants.DataBase.UNREAD, false);

        FirebaseDatabase.getInstance().getReference().updateChildren(unreadMap);
    }

    public MessengerAdapter(Context context, final String chatKey, final String lastMessageKey, final Map<String, User> members, final MessengerFragment.OnListFragmentInteractionListener listener) {
        this.members = members;

        mListener = listener;
        currentUser = User.getCurrentUser(context);

        childEventListener = new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (!checkMessageSnapshot(dataSnapshot))
                    return;

                Message message = dataSnapshot.getValue(Message.class);
                message.setKey(dataSnapshot.getKey());

                if ( !messageList.contains(message) ) {
                    addMessage(message);

                    //messageList.add(message);
                    //Collections.sort(messageList);
                    //notifyItemInserted(messageList.indexOf(message));

                    if (lastMessageKey != null && message.getKey().equals(lastMessageKey))
                        listener.onLastItemLoaded();
                }

                removeUnread(chatKey);

                if (message.isState(Message.STATE_UNREAD)) {
                    if (!message.getSender().equals(currentUser.getUid())) {
                        FirebaseDatabase.getInstance().getReference()
                                .child(Constants.DataBase.MESSAGES)
                                .child(chatKey)
                                .child(message.getKey())
                                .child(Constants.DataBase.STATE)
                                .setValue(Message.STATE_NORMAL);
                    }
                }

                listener.onListFragmentItemReceived(message);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (!checkMessageSnapshot(dataSnapshot))
                    return;

                Message message = dataSnapshot.getValue(Message.class);
                message.setKey(dataSnapshot.getKey());

                int index = messageList.indexOf(message);

                if (index > -1) {
                    boolean isSameState = messageList.get(index).getState() == message.getState();

                    messageList.set(index, message);

                    if (!isSameState)
                        notifyItemChanged(index);

                } else {
                    addMessage(message);
                    if (lastMessageKey != null && message.getKey().equals(lastMessageKey))
                        listener.onLastItemLoaded();
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        databaseReference = FirebaseDatabase.getInstance()
                .getReference()
                .child(Constants.DataBase.MESSAGES)
                .child(chatKey);

        databaseReference.keepSynced(true);
        databaseReference.limitToLast(COUNT_ITEMS_LOAD).addChildEventListener(childEventListener);
    }

    public void addMessage(Message message) {
        messageList.add(message);
        Collections.sort(messageList);
        notifyItemInserted(messageList.indexOf(message));
        //messageList.add(message);
        //notifyItemInserted(messageList.size() - 1);
    }

    private boolean checkMessageSnapshot(DataSnapshot messageSnapshot) {
        return messageSnapshot.hasChild(Constants.DataBase.MESSAGE)
                && messageSnapshot.hasChild(Constants.DataBase.SENDER)
                && messageSnapshot.hasChild(Constants.DataBase.STATE)
                && messageSnapshot.hasChild(Constants.DataBase.TIMESTAMP);
    }

    public void changeMessage(Message message) {
        int position = messageList.indexOf(message);
        messageList.set(messageList.indexOf(message), message);
        notifyItemChanged(position);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = messageList.get(position);
        User user = members.get(holder.mItem.getSender());
        holder.mContentView.setText(messageList.get(position).getMessage());

        if ( holder.getItemViewType() == R.layout.item_message_unread ) {

            if (holder.mItem.isState(Message.STATE_PROGRESS))
                holder.progressBar.setVisibility(View.VISIBLE);
            else
                holder.progressBar.setVisibility(View.GONE);

        } else if ( holder.getItemViewType() == R.layout.item_message_left ) {
            if (!user.isCurrentUser()) {
                holder.tvSymbol.setText(user.getSymbolAvatar());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        User user = members.get(messageList.get(position).getSender());

        Message message = messageList.get(position);
        if (message.isState(Message.STATE_FAIL))
            return R.layout.item_mess_fail_right;
        else if (message.isState(Message.STATE_PROGRESS) || message.isState(Message.STATE_UNREAD))
            return R.layout.item_message_unread;
        else if (user.isCurrentUser())
            return R.layout.item_message_right;
        else
            return R.layout.item_message_left;
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public Message getItem(int position) {
        return messageList.get(position);
    }

    public int getItemPosition(Message message) {
        return messageList.indexOf(message);
    }

    public void cleanupListener() {
        if (childEventListener != null) {
            databaseReference.removeEventListener(childEventListener);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final CircleImageView civAvatar;
        public final TextView tvSymbol;
        public final TextView mContentView;
        public final ProgressBar progressBar;
        public Message mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);
            civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
            mContentView = (TextView) view.findViewById(R.id.tvMessage);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

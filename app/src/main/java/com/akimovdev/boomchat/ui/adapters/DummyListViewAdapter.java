package com.akimovdev.boomchat.ui.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.akimovdev.boomchat.R;

import java.util.ArrayList;
import java.util.List;


//public class DummyListViewAdapter extends BaseAdapter {
public class DummyListViewAdapter extends RecyclerView.Adapter<DummyListViewAdapter.ViewHolder> {

    private Context mContext;
    private List<String> mDummyStrings;

    public DummyListViewAdapter(Context mContext) {
        this.mContext = mContext;
        mDummyStrings = getDummyStrings();
    }

    //@Override
    //public int getCount() {
        //return mDummyStrings.size();
    //}

    @Override
    public int getItemCount() {
        return mDummyStrings.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.listview_cell, parent, false);
        return new DummyListViewAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.mCellNumber.setText("" + position);
        holder.mCellText.setText(mDummyStrings.get(position));
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    /*@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if(convertView==null){
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

            convertView = inflater.inflate(R.layout.listview_cell, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.mCellNumber = (TextView) convertView.findViewById(R.id.cell_number);
            viewHolder.mCellText = (TextView) convertView.findViewById(R.id.cell_text);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.mCellNumber.setText("" + position);
        viewHolder.mCellText.setText(mDummyStrings.get(position));

        return convertView;
    } */

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView mCellNumber;
        TextView mCellText;

        public ViewHolder(View itemView) {
            super(itemView);

            mCellNumber = (TextView) itemView.findViewById(R.id.cell_number);
            mCellText = (TextView) itemView.findViewById(R.id.cell_text);
        }
    }

    public List<String> getDummyStrings() {
        List<String> dummyStrings = new ArrayList<>();

        dummyStrings.add("You want");
        dummyStrings.add("to test");
        dummyStrings.add("this library");
        dummyStrings.add("from both");
        dummyStrings.add("direction.");
        dummyStrings.add("You may");
        dummyStrings.add("be amazed");
        dummyStrings.add("when done");
        dummyStrings.add("so!");
        dummyStrings.add("I am");
        dummyStrings.add("going to");
        dummyStrings.add("add a little");
        dummyStrings.add("more lines");
        dummyStrings.add("for big");
        dummyStrings.add("smartphones.");

        return dummyStrings;
    }
}

package com.akimovdev.boomchat.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.ui.activities.AuthorizationActivity;
import com.akimovdev.boomchat.ui.activities.MainActivity;
import com.akimovdev.boomchat.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;


public class SignUpFragment extends BaseFragment<AuthorizationActivity> {

    private String TAG = "SIGN_UP_FRAGMENT";

    private FirebaseAuth mAuth;

    private EditText etDisplayName;
    private EditText etEmail;
    private EditText etPassword;
    private Button buttonSignUp;


    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {}

        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

        setHasOptionsMenu(true);

        etDisplayName = (EditText) view.findViewById(R.id.etFullName);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        buttonSignUp = (Button) view.findViewById(R.id.buttonSignUp);

        buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser( etEmail.getText().toString(),
                        etPassword.getText().toString(),
                        etDisplayName.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getHostActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void createUser(final String email, final String password, final String displayName) {

        String validMessage = getHostActivity().validateForm(email, password, displayName);
        if (validMessage != null) {
            Toast.makeText(getContext(), validMessage, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!getHostActivity().isNetworkConnected()) {
            onAuthFail(getString(R.string.alert_sending_package_disable));
            return;
        }

        getHostActivity().showProgressDialog();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Toast.makeText(getHostActivity(), "onFailure",
                                //Toast.LENGTH_SHORT).show();
                        //onAuthFail(getString(R.string.alert_auth_failed));
                        getHostActivity().hideProgressDialog();
                        onAuthFail(e.getMessage());
                    }
                })
                .addOnCompleteListener(getHostActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            saveUser(task, email, password, displayName);
                        } /*else {
                            getHostActivity().hideProgressDialog();

                            onAuthFail(getString(R.string.alert_auth_failed));
                            //Toast.makeText(getHostActivity(), R.string.alert_auth_failed,
                                    //Toast.LENGTH_SHORT).show();
                        }*/
                    }
                });
    }


    private void saveUser(@NonNull Task<AuthResult> task, String email, String password, String displayName) {

        final FirebaseUser firebaseUser = task.getResult().getUser();

        getHostActivity().saveUser(firebaseUser, email, password, displayName, null, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                getHostActivity().hideProgressDialog();

                if (!task.isSuccessful()) {
                    Toast.makeText(getHostActivity(), "Record error", Toast.LENGTH_LONG).show();
                } else {
                    //Intent intent = new Intent(getHostActivity(), MainActivity.class);
                    //startActivity(intent);
                    //getHostActivity().finish();
                    sendUID(firebaseUser.getUid());
                }
            }
        });
    }

    private void sendUID(final String uid) {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = Constants.URL_SERVER +"senduid.php";
        //String url ="http://s673212913.onlinehome.us/senduid";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        getHostActivity().hideProgressDialog();
                        //isAuthStart = false;
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getHostActivity().hideProgressDialog();
                //onAuthFail(getString(R.string.alert_auth_internet_not_available));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                String androidId = MessengerApplication.getAndroidID(getContext().getContentResolver());
                //String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

                Map<String, String>  params = new HashMap<String, String>();

                params.put("uid", uid);
                params.put("duid", androidId);

                return params;
            }
        };

        queue.add(stringRequest);
    }


    private void onAuthFail(String alert) {

        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            FirebaseAuth.getInstance().signOut();

        getHostActivity().hideProgressDialog();
        getHostActivity().showAlertDialog(getString(R.string.alert_auth_failed), alert);
    }
}

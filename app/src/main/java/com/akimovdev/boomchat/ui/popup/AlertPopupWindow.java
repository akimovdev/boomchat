package com.akimovdev.boomchat.ui.popup;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.akimovdev.boomchat.R;

public class AlertPopupWindow extends BasePopupWindow {

    protected AlertPopupWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);
    }

    public static AlertPopupWindow create(Activity activity) {

        DisplayMetrics metrics = getDisplaySize(activity);

        LayoutInflater inflater =
                (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View layout = inflater.inflate(R.layout.dialog_alert, (ViewGroup) activity.findViewById(R.id.popup_element));

        final AlertPopupWindow popup = new AlertPopupWindow (layout, metrics.widthPixels, metrics.heightPixels, true);

        return popup;
    }
}

package com.akimovdev.boomchat.ui.adapters;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akimovdev.boomchat.model.Friend;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.FriendListFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private final static int WHAT_LOAD_FINISHED = 1;

    private final static int MODE_SEARCH = 0;
    private final static int MODE_CURRENT = 1;

    private List<User> mValues;

    private final List<User> allList;
    private final List<User> favoriteList;
    private final List<User> searchList;
    private User currentUser;

    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;
    private final FriendListFragment.OnListFragmentInteractionListener mListener;
    private int mode = MODE_CURRENT;
    private int sort = Friend.STATE_APPROVED;

    private Handler myHandle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == WHAT_LOAD_FINISHED) {
                if (mListener != null) mListener.onLoadComplete(mValues.size());
                    notifyDataSetChanged();
            }
        }
    };

    public FriendsAdapter(Context context, FriendListFragment.OnListFragmentInteractionListener listener) {
        allList = new ArrayList<>();
        favoriteList = new ArrayList<>();
        searchList = new ArrayList<>();

        mValues = favoriteList;

        mListener = listener;
        currentUser = User.getCurrentUser(context);

        loadFriends();
    }

    public void loadFriends() {
        mode = MODE_CURRENT;
        notifyDataSetChanged();
        databaseReference = FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS);
        valueEventListener = getFriendsEventListener();
        databaseReference.keepSynced(true);
        //databaseReference.addListenerForSingleValueEvent(valueEventListener);
        databaseReference.addValueEventListener(valueEventListener);

    }


    private ValueEventListener getFriendsEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                parseLoadedFriends(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (mListener != null)
                    mListener.onFail(databaseError.getMessage());
            }
        };
    }

    private boolean checkUserSnapshot(DataSnapshot userSnapshot) {
        return userSnapshot.hasChild(Constants.DataBase.DISPLAY_NAME)
                && userSnapshot.hasChild(Constants.DataBase.EMAIL)
                && userSnapshot.hasChild(Constants.DataBase.UID);
    }

    private void parseLoadedFriends(final DataSnapshot dataSnapshot) {
        allList.clear();
        favoriteList.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {

                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {

                    if (checkUserSnapshot(userSnapshot)) {
                        User friend = userSnapshot.getValue(User.class);
                        friend.setUid(userSnapshot.getKey());

                        sortItem(friend);
                    }
                }

               myHandle.sendEmptyMessage(WHAT_LOAD_FINISHED);
            }
        }).start();
    }

    private void sortItem(User item) {

        if (mode == MODE_SEARCH) {
            searchList.add(item);
            return;
        }

        if (item.isCurrentUser())
            return;

        allList.add(item);

        if (currentUser.isFriend(item.getUid())) {
            favoriteList.add(item);
        }
    }

    public void removeFromFavorite(User user) {

        int position = mValues.indexOf(user);

        if (sort == Friend.STATE_NO_RELATIONS) {
            notifyItemChanged(position);
        } else if(sort == Friend.STATE_APPROVED) {
            mValues.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addToFavorite(User user, int position) {

        if ( !currentUser.isFriend(user.getUid()) )
            return;

        if (sort == Friend.STATE_NO_RELATIONS) {
            notifyItemChanged(position);
        } else if (sort == Friend.STATE_APPROVED) {
            mValues.add(user);
            notifyItemRemoved(position);
        }
    }

    private void parseSearchResult(final DataSnapshot dataSnapshot) {

        new Thread(new Runnable() {
            @Override
            public void run() {

                for (DataSnapshot searchSnapshot : dataSnapshot.getChildren()) {

                    FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS)
                            .child(searchSnapshot.getKey())
                            .addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User friend = dataSnapshot.getValue(User.class);
                                    friend.setUid(dataSnapshot.getKey());

                                    sortItem(friend);
                                    myHandle.sendEmptyMessage(WHAT_LOAD_FINISHED);
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {
                                    if (mListener != null)
                                        mListener.onFail(databaseError.getMessage());
                                }
                            });
                }
            }
        }).start();
    }

    public void searchFriends(String searchWorld) {

        mValues = searchList;
        mValues.clear();

        mode = MODE_SEARCH;
        notifyDataSetChanged();

        if (searchWorld.isEmpty()) {
            if (mListener != null)
                mListener.onListFragmentCount(0);

            return;
        }

        databaseReference = FirebaseDatabase.getInstance().getReference()
                .child(Constants.DataBase.SEARCH)
                .child(Constants.DataBase.USERS);

        databaseReference.orderByChild(searchWorld)
                .equalTo(true)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        parseSearchResult(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        if (mListener != null)
                            mListener.onFail(databaseError.getMessage());
                    }
                });

        databaseReference.orderByChild(searchWorld)
                .equalTo(true).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (null != mListener) {
                    mListener.onListFragmentCount(dataSnapshot.getChildrenCount());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                if (mListener != null)
                    mListener.onFail(databaseError.getMessage());
            }
        });
    }

    public void clear() {
        mValues.clear();
        if (sort == Friend.STATE_APPROVED)
            mValues = favoriteList;
        else
            mValues = allList;
    }

    public void sortFavorite() {
        sort = Friend.STATE_APPROVED;
        mValues = favoriteList;

        notifyDataSetChanged();
    }

    public void sortAll() {
        sort = Friend.STATE_NO_RELATIONS;
        mValues = allList;

        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_friend, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        MessengerApplication.getInstance().displayAvatar(holder.mItem.getPhotoUrl(), holder.civAvatar);

        holder.tvDisplayName.setText(mValues.get(position).getDisplayName());
        holder.tvSymbol.setText(holder.mItem.getSymbolAvatar());

        if (sort == Friend.STATE_APPROVED && mode == MODE_CURRENT) {
            holder.ivMark.setVisibility(View.GONE);
        } else {
            if (currentUser.isFriend(holder.mItem.getUid()))
                holder.ivMark.setVisibility(View.VISIBLE);
            else
                holder.ivMark.setVisibility(View.GONE);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onListFragmentInteraction(holder.mItem, position, holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void cleanupListener() {
        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final CircleImageView civAvatar;
        public final TextView tvSymbol;
        public final TextView tvDisplayName;
        public final ImageView ivMark;

        public User mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
            ivMark = (ImageView) view.findViewById(R.id.ivMark);
            tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);
            tvDisplayName = (TextView) view.findViewById(R.id.tvDisplayName);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvDisplayName.getText() + "'";
        }
    }
}

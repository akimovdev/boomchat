package com.akimovdev.boomchat.ui.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.akimovdev.boomchat.databinding.FragmentMessengerBinding;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.Message;
import com.akimovdev.boomchat.services.ServiceReceiver;
import com.akimovdev.boomchat.ui.adapters.MessengerAdapter;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.services.SenderService;
import com.akimovdev.boomchat.utils.PoolTransporter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import java.util.HashMap;
import java.util.Map;


public class MessengerFragment extends BaseFragment implements ServiceReceiver.Receiver, SwipyRefreshLayout.OnRefreshListener {

    private static final String TAG = "MessengerFragment";
    private static final String ARG_CHAT = "chat";

    private static final int DURATION_START_SWIPE = 1000;

    private String chatKey;
    private Chat chat;
    private Map<String, User> members;
    private User contactUser;
    private OnListFragmentInteractionListener mListener;
    private ServiceReceiver serviceReceiver;
    private Runnable runnableSwipe;
    private Handler handlerSwipe;

    private EditText etMessage;
    private RecyclerView rvMessages;
    private ImageView ivSend;
    private TextView tvEmptyList;
    private LinearLayoutManager linearLayoutManager;
    private SwipyRefreshLayout mSwipeRefreshLayout;

    boolean hasTurn = false;

    public MessengerFragment() {}

    public static MessengerFragment newInstance(Chat chat, User currentUser, User contactUser) {

        Map<String, User> members = new HashMap<>();
        members.put(contactUser.getUid(), contactUser);
        members.put(currentUser.getUid(), currentUser);

        MessengerFragment fragment = new MessengerFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_CHAT, chat);
        PoolTransporter.getInstance().push(Constants.Extras.MEMBERS, members);
        PoolTransporter.getInstance().push(Constants.Extras.CONTACT_USER, contactUser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {

            chat = getArguments().getParcelable(ARG_CHAT);

            if (chat != null)
                chatKey = chat.getKey();

            members = (Map<String, User>) PoolTransporter.getInstance().pop(Constants.Extras.MEMBERS);
            contactUser = (User) PoolTransporter.getInstance().pop(Constants.Extras.CONTACT_USER);
        }

        if (members == null)
            throw new Error("Members can not be null !!!");

        if (contactUser == null)
            throw new Error("Contact user can not be null !!!");

        serviceReceiver = new ServiceReceiver(new Handler());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentMessengerBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_messenger, container, false);
        final View view = binding.getRoot();

        runnableSwipe = createStartSwipe();
        handlerSwipe = new Handler();

        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(contactUser.getDisplayName());

        mSwipeRefreshLayout = (SwipyRefreshLayout) view.findViewById(R.id.refresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setEnabled(false);

        etMessage = (EditText) view.findViewById(R.id.etText);

        ivSend = (ImageView) view.findViewById(R.id.ivSend);
        ivSend.setEnabled(false);
        ivSend.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider));
        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (hasTurn) {
                    Toast.makeText(getContext(), getString(R.string.alert_message_turn), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (etMessage.getText().toString().isEmpty()) {
                    Toast.makeText(view.getContext(), getString(R.string.alert_enter_message), Toast.LENGTH_SHORT).show();
                    return;
                } else if(!getHostActivity().isNetworkConnected()) {
                    getHostActivity().showAlertDialog(getString(R.string.title_sending_package_disable),
                                                        getString(R.string.alert_sending_package_disable));
                    return;
                }

                hasTurn = true;
                sendMessage(getHostActivity(), etMessage.getText().toString());
            }
        });


        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etMessage.getText().toString().isEmpty()) {
                    ivSend.setEnabled(false);
                    ivSend.setColorFilter(ContextCompat.getColor(getContext(), R.color.divider));
                } else {
                    ivSend.setEnabled(true);
                    ivSend.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        tvEmptyList = (TextView) view.findViewById(R.id.tvEmptyList);

        Context context = view.getContext();
        rvMessages = (RecyclerView) view.findViewById(R.id.rvSendItems);

        linearLayoutManager = new LinearLayoutManager(context);
        linearLayoutManager.setStackFromEnd(true);
        rvMessages.setLayoutManager(linearLayoutManager);

        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentItemReceived(Message item) {
                tvEmptyList.setVisibility(View.GONE);
            }

            @Override
            public void onLastItemLoaded() {
                cancelSwipeRefresh();
            }
        };

        if (chatKey != null) {
            rvMessages.setAdapter(new MessengerAdapter(view.getContext(), chatKey, chat.getLastMessageKey(), members, mListener));
            initAdapterObserver();
            cancelNotification();
            removeUnread();

        } else {

            cancelSwipeRefresh();
            tvEmptyList.setVisibility(View.VISIBLE);
            tvEmptyList.setVisibility(View.VISIBLE);

            //chatKey = FirebaseDatabase.getInstance().getReference()
                    //.child(Constants.DataBase.CHATS).child(contactUser.getUid()).push().getKey();

            chatKey = createChatKey();

            rvMessages.setAdapter(new MessengerAdapter(view.getContext(), chatKey, null, members, mListener));
            initAdapterObserver();
            cancelNotification();
        }

        return view;
    }

    private void cancelSwipeRefresh() {
        mSwipeRefreshLayout.setRefreshing(false);

        handlerSwipe.removeCallbacks(runnableSwipe);
        runnableSwipe = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!serviceReceiver.isSetReceiver())
            serviceReceiver.setReceiver(this);

        MessengerApplication.getInstance().chatIn();
    }

    private void cancelNotification() {
        NotificationManager notificationManager = (NotificationManager) getHostActivity().getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(Constants.NOTIFICATION_CHAT_ID);
    }

    private void removeUnread() {

        User currentUser = User.getCurrentUser(getContext());
        FirebaseDatabase.getInstance().getReference()
                .child(Constants.DataBase.USERS)
                .child(currentUser.getUid())
                .child(Constants.DataBase.UNREAD)
                .child(chatKey)
                .setValue(null);
    }

    private void initAdapterObserver() {
        final MessengerAdapter adapter = (MessengerAdapter) rvMessages.getAdapter();
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                rvMessages.scrollToPosition(rvMessages.getAdapter().getItemCount()-1);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!getHostActivity().isNetworkConnected()) {
            tvEmptyList.setText(getString(R.string.no_internet_connection));
            tvEmptyList.setVisibility(View.VISIBLE);
        } else {
            tvEmptyList.setText(getString(R.string.empty_message));
        }

        if (runnableSwipe != null)
            handlerSwipe.postDelayed(runnableSwipe, DURATION_START_SWIPE);
    }

    private Runnable createStartSwipe() {
        return new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
            }
        };
    }

    @Override
    public void onStop() {
        super.onStop();
        serviceReceiver.setReceiver(null);

        MessengerApplication.getInstance().chatOut();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        MessengerAdapter adapter = (MessengerAdapter) rvMessages.getAdapter();
        adapter.cleanupListener();

        cancelSwipeRefresh();
    }

    private void putMessageToList(Message message) {

        etMessage.getText().clear();
        tvEmptyList.setVisibility(View.GONE);

        MessengerAdapter adapter = (MessengerAdapter) rvMessages.getAdapter();
        adapter.addMessage(message);

        rvMessages.scrollToPosition(rvMessages.getAdapter().getItemCount()-1);
    }

    private void sendMessage(final Context context, final String messageBody) {
        DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
        User currentUser = User.getCurrentUser(getContext());

        String messageKey = mDatabase.child(Constants.DataBase.MESSAGES).child(chatKey).push().getKey();

        Message message = new Message();
        message.setKey(messageKey);
        message.setSender(currentUser.getUid());
        message.setState(Message.STATE_PROGRESS);
        message.setMessage(messageBody);

        SenderService.startActionSendMessage(context, chatKey, message, contactUser, currentUser, serviceReceiver);

        putMessageToList(message);
    }

    private String createChatKey() {
        //return createStandardKey();
        return createByUidKey();
    }

    private String createStandardKey() {
        return FirebaseDatabase.getInstance().getReference().push().getKey();
    }

    private String createByUidKey() {

        User currentUser = User.getCurrentUser(getContext());

        String currentUid = currentUser.getUid();
        String contactUid = contactUser.getUid();
        int compareResult = currentUid.compareTo(contactUid);

        String key = null;

        if ( compareResult > 0 )
            key = "-CH_"+currentUser.getUid()+"_"+contactUser.getUid();
            //return "-CH_"+currentUser.getUid()+"_"+contactUser.getUid();
        else
            key = "-CH_"+contactUser.getUid()+"_"+currentUser.getUid();

        return String.valueOf(key.hashCode());
    }

    private void onSuccessSend(Message message) {
        hasTurn = false;

        MessengerAdapter adapter = (MessengerAdapter) rvMessages.getAdapter();
        int position = adapter.getItemPosition(message);

        View view = linearLayoutManager.findViewByPosition(position);

        if (view != null) {
            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
        }

        adapter.getItem(position).setState(Message.STATE_UNREAD);
    }

    private void onErrorSend(Message message) {
        hasTurn = false;

        if (message == null)
            throw new Error("Message can not be null");

        MessengerAdapter adapter = (MessengerAdapter) rvMessages.getAdapter();

        int position = adapter.getItemPosition(message);
        message.setMessage(getString(R.string.message_fail));
        message.setState(Message.STATE_FAIL);
        adapter.changeMessage(message);

        View view = linearLayoutManager.findViewByPosition(position);
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultCode == Constants.RESULT_RECEIVER_MESSAGE_SUCCESS) {
            Message message = resultData.getParcelable(Constants.Extras.MESSAGE);
            onSuccessSend(message);
        } else if (resultCode == Constants.RESULT_RECEIVER_MESSAGE_ERROR) {
            Message message = resultData.getParcelable(Constants.Extras.MESSAGE);
            onErrorSend(message);
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {

    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentItemReceived(Message item);
        void onLastItemLoaded();
    }
}

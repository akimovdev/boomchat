package com.akimovdev.boomchat.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.ui.fragments.MessengerFragment;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.utils.PoolTransporter;

public class MessengerActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_base);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState != null)
            return;

        Chat chat = getIntent().getParcelableExtra(Constants.Extras.CHAT);

        User user = (User) PoolTransporter.getInstance().pop(Constants.Extras.CONTACT_USER);
        addFragment(MessengerFragment.newInstance(chat, User.getCurrentUser(this), user));
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }

    public static void startNewChatActivity(Context context, User contactUser, Chat chat) {
        Intent intent = new Intent(context, MessengerActivity.class);
        intent.putExtra(Constants.Extras.CHAT, chat);
        PoolTransporter.getInstance().push(Constants.Extras.CONTACT_USER, contactUser);
        context.startActivity(intent);
    }
}

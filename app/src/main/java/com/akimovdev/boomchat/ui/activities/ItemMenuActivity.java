package com.akimovdev.boomchat.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.akimovdev.boomchat.ui.fragments.BaseFragment;
import com.akimovdev.boomchat.ui.fragments.ChatListFragment;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.ui.fragments.SettingsFragment;

import com.akimovdev.boomchat.ui.fragments.AboutFragment;

public class ItemMenuActivity extends BaseActivity {

    private OnFragmentBackListener fragmentBackListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState != null)
            return;

        int idMenu = getIntent().getExtras().getInt(Constants.Extras.MAIN_MENU_ITEM_ID, -1);
        //int idMenu = Constants.MainMenu.ITEM_MESSAGES;

        /*if (idMenu == -1)
            throw new Error("Id menu is not initialized !!!"); */

        addFragment(selectFragment(idMenu));
    }


    public BaseFragment selectFragment(int id) {

        BaseFragment fragment = null;

        if (id == Constants.MainMenu.ITEM_MESSAGES) {
            fragment = new ChatListFragment();
        } else if (id == Constants.MainMenu.ITEM_REQUEST) {
            fragment = new  AboutFragment();
        } else if (id == Constants.MainMenu.ITEM_SETTINGS) {
            fragment = SettingsFragment.newInstance();
        }

        return fragment;
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }

    @Override
    public void onBackPressed() {
        if (fragmentBackListener == null || fragmentBackListener.onBackPressed())
            super.onBackPressed();
    }

    public static void startItemActivity(Context context, int idMenu) {
        Intent intent = new Intent(context, ItemMenuActivity.class);
        intent.putExtra(Constants.Extras.MAIN_MENU_ITEM_ID, idMenu);
        context.startActivity(intent);
    }
}

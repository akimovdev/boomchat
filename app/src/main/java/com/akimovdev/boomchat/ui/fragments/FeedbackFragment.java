package com.akimovdev.boomchat.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.model.request.firebase.FirebaseTimeResponseRequest;
import com.akimovdev.boomchat.model.request.firebase.OnSuccessTimeUpListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class FeedbackFragment extends BaseFragment {

    private TextView tvSender;
    private TextView etFeedback;

    private FirebaseTimeResponseRequest request;
    private User currentUser;

    private FloatingActionButton fab;

    public FeedbackFragment() {
        // Required empty public constructor
    }

    public static FeedbackFragment newInstance() {
        FeedbackFragment fragment = new FeedbackFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        currentUser = User.getCurrentUser(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_feedback, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvSender = (TextView) view.findViewById(R.id.tvSender);
        etFeedback = (EditText) view.findViewById(R.id.etFeedback);

        tvSender.setText(currentUser.getDisplayName());

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                send(etFeedback.getText().toString());
            }
        });
    }

    private void send(String message) {

        if (message == null || message.isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.alert_empty_feedback), Toast.LENGTH_SHORT).show();
            return;
        }

        if ( !getHostActivity().isNetworkConnected() ) {

            getHostActivity().showAlertDialog(getString(R.string.title_sending_package_disable),
                    getString(R.string.alert_sending_package_disable));

            return;
        }

        User currentUser = User.getCurrentUser(getContext());
        DatabaseReference feedbackReference = FirebaseDatabase.getInstance().getReference()
                                                .child(Constants.DataBase.FEEDBACK)
                                                .child(currentUser.getUid());

        String feedbackKey = feedbackReference.push().getKey();

        request = FirebaseTimeResponseRequest.createRequest( feedbackReference
                                                        .child(feedbackKey)
                                                        .child(Constants.DataBase.MESSAGE) );

        getHostActivity().showProgressDialog();


        OnSuccessTimeUpListener listener = new OnSuccessTimeUpListener() {

            @Override
            public void onSuccess(Void aVoid) {
                super.onSuccess(aVoid);
                onSuccessResponse();
            }

            @Override
            public void onTimeUpResponse() {
                onFailResponse();
            }
        };

        request.setValue(message, getHostActivity(), listener);
    }

    private void onSuccessResponse() {
        getHostActivity().hideProgressDialog();
        getHostActivity().setResult(Activity.RESULT_OK, createResult(Constants.FEEDBACK_RESULT_SUCCESS));
        getHostActivity().finish();
    }

    private void onFailResponse() {
        getHostActivity().hideProgressDialog();
        getHostActivity().setResult(Activity.RESULT_OK, createResult(Constants.FEEDBACK_RESULT_FAIL));
        getHostActivity().finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (request != null)
            request.stopWaitResponse();
    }

    private Intent createResult(int result) {
        Intent data = new Intent();
        data.putExtra(Constants.Extras.FEEDBACK_RESULT, result);

        return data;
    }
}

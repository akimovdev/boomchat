package com.akimovdev.boomchat.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.DashboardFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;

public class MainActivity extends BaseActivity {

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private static boolean isRun = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toolbar_overlay);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        isRun = true;

        checkPlayServices();

        User currentUser = User.getCurrentUser(this);

        if (currentUser == null) {
            logout();
            return;
        }

        launchByFlags();

        if (savedInstanceState != null)
            return;

        addFragment(new DashboardFragment());

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();

                if (user == null) {
                    logout();
                }
            }
        };
    }

    private void launchByFlags() {

        int flags = getIntent().getFlags();
        boolean isLaunchedFromHistory = (flags & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0;

        if ( isLaunchedFromHistory )
            return;

        if ( getIntent().hasExtra(Constants.Extras.CHAT) &&
                getIntent().hasExtra(Constants.Extras.CONTACT_USER) ) {

            Chat chat = getIntent().getParcelableExtra(Constants.Extras.CHAT);
            User contactUser = getIntent().getParcelableExtra(Constants.Extras.CONTACT_USER);

            if (chat != null && contactUser != null)
                MessengerActivity.startNewChatActivity(this, contactUser, chat );

        }
    }

    private void logout() {

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        startActivity(new Intent(this, AuthorizationActivity.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mAuth != null && mAuthListener != null)
            mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuth!= null && mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isRun = false;
    }

    public static boolean isRun() {
        return isRun;
    }



    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        finish();
                    }
                }).show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }
}

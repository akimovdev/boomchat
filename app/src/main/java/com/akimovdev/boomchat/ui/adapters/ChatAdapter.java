package com.akimovdev.boomchat.ui.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.ChatListFragment;
import com.akimovdev.boomchat.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private final List<Chat> values;
    private final ChatListFragment.OnListFragmentInteractionListener listener;
    private User currentUser;
    private DatabaseReference databaseReference;
    private ChildEventListener childEventListener;

    public ChatAdapter(Context context, DatabaseReference databaseReference, ChatListFragment.OnListFragmentInteractionListener listener) {
        values = new ArrayList<>();
        this.listener = listener;
        currentUser = User.getCurrentUser(context);

        this.databaseReference = databaseReference;

        childEventListener = createChildEventListener();
        databaseReference.orderByChild(Constants.DataBase.TIMESTAMP).addChildEventListener(childEventListener);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = values.get(position);
        String sign = holder.mItem.getDisplayName();

        holder.tvLastMessage.setText(holder.mItem.getLastMessage());
        holder.tvSymbol.setText(User.getSymbolAvatar(sign));
        holder.tvDisplayName.setText(holder.mItem.getDisplayName());
        MessengerApplication.getInstance().displayAvatar(holder.mItem.getPhotoUrl(), holder.civAvatar);

        if ( holder.mItem.isUnread() && !currentUser.getUid().equals(holder.mItem.getLastSender()) )
            holder.indicator.setVisibility(View.VISIBLE);
        else
            holder.indicator.setVisibility(View.GONE);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {

                    User user = new User();
                    user.setUid(holder.mItem.getReceiver());

                    if (holder.mItem.getPhotoUrl() != null)
                        user.setPhotoUri(Uri.parse(holder.mItem.getPhotoUrl()));

                    user.setDisplayName(holder.mItem.getDisplayName());

                    listener.onListFragmentInteraction(holder.mItem, currentUser, user);
                }
            }
        });
    }

    private boolean checkChatSnapshot(DataSnapshot dataSnapshot) {
        return dataSnapshot.hasChild(Constants.DataBase.DISPLAY_NAME)
                && dataSnapshot.hasChild(Constants.DataBase.LAST_MESSAGE)
                && dataSnapshot.hasChild(Constants.DataBase.LAST_MESSAGE_KEY)
                && dataSnapshot.hasChild(Constants.DataBase.LAST_SENDER)
                && dataSnapshot.hasChild(Constants.DataBase.RECEIVER)
                && dataSnapshot.hasChild(Constants.DataBase.UNREAD);
    }

    private ChildEventListener createChildEventListener() {
        return new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (!checkChatSnapshot(dataSnapshot))
                    return;

                Chat chat = dataSnapshot.getValue(Chat.class);
                chat.setKey(dataSnapshot.getKey());

                values.add(chat);
                notifyItemInserted(values.indexOf(chat));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                if (!checkChatSnapshot(dataSnapshot))
                    return;

                Chat chat = dataSnapshot.getValue(Chat.class);
                chat.setKey(dataSnapshot.getKey());
                int index = values.indexOf(chat);

                if (index == -1) {
                    values.add(chat);
                    notifyItemInserted(values.indexOf(chat));
                } else {
                    values.set(index, chat);
                    notifyItemChanged(index);
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                Log.d("MOVED", "onMoved");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public void cleanupListener() {
        if (childEventListener != null) {
            databaseReference.removeEventListener(childEventListener);
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final View indicator;
        public final CircleImageView civAvatar;
        public final TextView tvSymbol;
        public final TextView tvDisplayName;
        public final TextView tvLastMessage;
        public Chat mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
            tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);
            tvDisplayName = (TextView) view.findViewById(R.id.tvDisplayName);
            tvLastMessage = (TextView) view.findViewById(R.id.tvLastMessage);
            indicator = view.findViewById(R.id.indicator);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvLastMessage.getText() + "'";
        }
    }
}

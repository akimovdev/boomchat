package com.akimovdev.boomchat.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.activities.MessengerActivity;
import com.akimovdev.boomchat.ui.activities.ProfileActivity;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends BaseFragment<ProfileActivity> {

    private CircleImageView civAvatar;
    private TextView tvSymbol;
    private TextView tvDisplayName;
    private TextView tvCountFriends;
    private RelativeLayout rlEmail;
    private RelativeLayout rlChat;
    private RelativeLayout rlRelation;
    private ImageView icFavorite;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(/*Bundle args*/) {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(getString(R.string.title_screen_profile));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
        tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);
        tvDisplayName = (TextView) view.findViewById(R.id.tvDisplayName);
        tvCountFriends = (TextView) view.findViewById(R.id.tvCountFriends);
        rlEmail = (RelativeLayout) view.findViewById(R.id.blockUser);
        rlChat = (RelativeLayout) view.findViewById(R.id.rlChat);
        rlRelation = (RelativeLayout) view.findViewById(R.id.rlRelation);
        icFavorite = (ImageView) view.findViewById(R.id.ivAdd);
        buildView(getHostActivity().getUser());
    }

    private void onSendEmail(User user) {

        String viaGoogle = getString(R.string.via_google_plus);
        String email = user.getEmail();

        if (email.equals(viaGoogle)) {
            getHostActivity().showAlertDialog( getString(R.string.title_not_use_email),
                                            getString(R.string.alert_not_use_email) );
            return;
        } else if (!getHostActivity().isNetworkConnected()) {
            getHostActivity().showAlertDialog( getString(R.string.title_sending_package_disable),
                                            getString(R.string.alert_sending_package_disable) );
            return;
        }

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{user.getEmail()});
        i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.default_email_subject));
        i.putExtra(Intent.EXTRA_TEXT   , getString(R.string.default_email_body));
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));

        } catch (android.content.ActivityNotFoundException ex) {
            getHostActivity().showAlertDialog( getString(R.string.title_internet_no_available),
                    getString(R.string.alert_chek_internet_available) );
        }
    }

    private void onRelationAction() {

        if (!getHostActivity().isNetworkConnected()) {
            getHostActivity().showAlertDialog( getString(R.string.title_sending_package_disable),
                                            getString(R.string.alert_sending_package_disable) );

            return;
        }

        boolean isFavorite = getHostActivity().isFavorite();

        if (isFavorite) {
            getHostActivity().declineFriend();
        } else {
            getHostActivity().addFriend();
        }

        icFavorite.setSelected(!isFavorite);
    }

    private void onChatAction(User user) {

        User currentUser = User.getCurrentUser(getContext());

        Chat chat = null;

        if (currentUser.getChats().containsKey(user.getUid())) {
            chat = new Chat();
            String chatKey = currentUser.getChats().get(user.getUid());

            chat.setKey(chatKey);
            chat.setLastMessageKey( currentUser.getLastMessages().get(chatKey) );
        }

        MessengerActivity.startNewChatActivity(getContext(), user, chat);
    }

    private void buildView(final User user) {

        if (user.getPhotoUrl() != null)
            MessengerApplication.getInstance().displayAvatar(user.getPhotoUrl(), civAvatar);

        tvSymbol.setText(user.getSymbolAvatar());
        tvDisplayName.setText(user.getDisplayName());
        tvCountFriends.setText(user.getEmail());

        icFavorite.setSelected(getHostActivity().isFavorite());
        rlChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChatAction(user);
            }
        });

        rlRelation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRelationAction();
            }
        });

        rlEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSendEmail(getHostActivity().getUser());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}

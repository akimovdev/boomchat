package com.akimovdev.boomchat.ui.adapters;

/*public class ProfileDataAdapter extends RecyclerView.Adapter<ProfileDataAdapter.ViewHolder> {

    private final int INDEX_LOCATION = 0;
    private final int INDEX_EMAIL = 1;
    private final int INDEX_FRIENDS = 2;

    private List<String> values;
    private List<Integer> icons;


    public ProfileDataAdapter(User user) {
        values = new ArrayList<>();
        icons = new ArrayList<>();

        icons.add(INDEX_LOCATION, R.drawable.ic_location_on_24);
        values.add(INDEX_LOCATION, user.getLastLocation());

        icons.add(INDEX_EMAIL, R.drawable.ic_email_24);
        values.add(INDEX_EMAIL, user.getEmail());

        icons.add(INDEX_FRIENDS, R.drawable.ic_person_24);
        values.add(INDEX_FRIENDS, "Friends "+user.getCountFriends());
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.ivIcon.setImageResource(icons.get(position));
        holder.tvItem.setText(values.get(position));
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.item_profile_data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView ivIcon;
        public final TextView tvItem;
        public Message mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivIcon = (ImageView) view.findViewById(R.id.ivIcon);
            tvItem = (TextView) view.findViewById(R.id.tvItem);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvItem.getText() + "'";
        }
    }

} */

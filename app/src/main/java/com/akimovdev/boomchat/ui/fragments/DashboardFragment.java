package com.akimovdev.boomchat.ui.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.text.TextUtilsCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.akimovdev.boomchat.ItemOffsetDecoration;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.services.LoadFileService;
import com.akimovdev.boomchat.services.ServiceReceiver;
import com.akimovdev.boomchat.ui.activities.FeedbackActivity;
import com.akimovdev.boomchat.ui.activities.FriendsActivity;
import com.akimovdev.boomchat.ui.activities.MainActivity;
import com.akimovdev.boomchat.ui.adapters.MainMenuAdapter;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.ui.activities.ItemMenuActivity;
import com.akimovdev.boomchat.R;
//import MainMenuAdapter;
import com.akimovdev.boomchat.utils.FileUtils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.Calendar;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


public class DashboardFragment extends BaseFragment<MainActivity> implements ServiceReceiver.Receiver, ValueEventListener {

    private final static String TAG = "DASHBOARD_FRAGMENT";

    private static final int ITEM_CAPTURE_PHOTO = 0;
    private static final int ITEM_CHOOSE_FROM_GALLERY = 1;
    //private static final int REQUEST_CODE_CHOOSE_AVATAR = 2;
    //private static final int REQUEST_CODE_CAPTURE_PHOTO = 3;
    //private static final int REQUEST_CODE_CHOOSE_FROM_GALLERY = 4;

    private TextView tvSymbol;
    private TextView tvDisplayName;
    private CircleImageView civAvatar;
    private RecyclerView rvMenu;

    private ServiceReceiver loadFileReceiver;
    private DatabaseReference userDataReference;

    private GoogleApiClient googleApiClient;

    OnListFragmentInteractionListener listener;

    private File file;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public static DashboardFragment newInstance(String param1, String param2) {
        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (loadFileReceiver == null)
            loadFileReceiver = new ServiceReceiver(new Handler());

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        googleApiClient = new GoogleApiClient.Builder(getHostActivity())
                .enableAutoManage(getHostActivity() /* FragmentActivity */, null /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        User user = User.getCurrentUser(getHostActivity());
        userDataReference = FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS).child(user.getUid());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        User user = User.getCurrentUser(getHostActivity());
        setHasOptionsMenu(true);

        if (userDataReference != null)
            userDataReference.addValueEventListener(this);

        civAvatar = (CircleImageView) view.findViewById(R.id.civAvatar);
        tvDisplayName = (TextView) view.findViewById(R.id.tvDisplayName);
        rvMenu = (RecyclerView) view.findViewById(R.id.rvMenu);
        tvSymbol = (TextView) view.findViewById(R.id.tvSymbol);

        //if (user.getPhotoUrl() != null)
        MessengerApplication.getInstance().displayAvatar(user.getPhotoUrl(), civAvatar);

        tvDisplayName.setText(user.getDisplayName());
        tvSymbol.setText(user.getSymbolAvatar());
        listener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(int id) {

                if (id == Constants.MainMenu.ITEM_SHARE) {
                    share();
                    return;
                } else if (id == Constants.MainMenu.ITEM_EXIT) {
                    logout();
                    return;
                } else if (id == Constants.MainMenu.ITEM_FRIENDS) {
                    FriendsActivity.startFriendsActivity(getContext());
                    return;
                } else if (id == Constants.MainMenu.ITEM_SETTINGS) {
                    Intent intent = new Intent(getContext(), FeedbackActivity.class);
                    startActivityForResult(intent, Constants.REQUEST_CODE_FEEDBACK);
                    return;
                }

                ItemMenuActivity.startItemActivity(getHostActivity(), id);
            }
        };

        MainMenuAdapter menuAdapter = new MainMenuAdapter(getHostActivity(), user.getUnread(), listener);

        rvMenu.setLayoutManager(new GridLayoutManager(getHostActivity(), 3));
        rvMenu.addItemDecoration(new ItemOffsetDecoration(getHostActivity(), R.dimen.item_offset));
        rvMenu.setAdapter(menuAdapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!loadFileReceiver.isSetReceiver())
            loadFileReceiver.setReceiver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        loadFileReceiver.setReceiver(null);
        //cleanupListeners();
    }

    private void changeAvatar() {
        if (!getHostActivity().isNetworkConnected()) {
            getHostActivity().showAlertDialog( getString(R.string.title_sending_package_disable),
                    getString(R.string.alert_sending_package_disable));
            return;
        }

        createDialog().show();
    }

    private void logout() {
        cleanupListeners();
        Auth.GoogleSignInApi.signOut(googleApiClient);
        User.getCurrentUser(getContext()).logout();
    }

    private void share() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getText(R.string.send_to));
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.invite_to)));
    }

    protected void uploadFileToStorage(final Uri uriFile, final int requestCode) {
        LoadFileService.startActionLoadImage(getHostActivity(), uriFile, User.getCurrentUser(getHostActivity()), requestCode, loadFileReceiver);
    }

    protected void onReceiveAvatarCache(String imagePath, int requestCode) {
        MessengerApplication.getInstance().displayAvatar(imagePath, civAvatar);
    }

    private void handleFeedbackResult(Intent data) {
        int feedbackResult = data.getIntExtra(Constants.Extras.FEEDBACK_RESULT, Constants.FEEDBACK_RESULT_FAIL);

        if (feedbackResult == Constants.FEEDBACK_RESULT_SUCCESS)
            getHostActivity().showAlertDialog( getString(R.string.title_alert_feedback),
                                        getString(R.string.alert_feedback_success) );
        else
            getHostActivity().showAlertDialog( getString(R.string.title_alert_feedback),
                    getString(R.string.alert_feedback_fail));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        loadFileReceiver.setReceiver(this);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Constants.REQUEST_CODE_CHOOSE_FROM_GALLERY) {
                uploadFileToStorage(data.getData(), requestCode);
            } else if (requestCode == Constants.REQUEST_CODE_CAPTURE_PHOTO) {
                uploadFileToStorage(Uri.fromFile(file), requestCode);
            } else if (requestCode == Constants.REQUEST_CODE_FEEDBACK) {
                handleFeedbackResult(data);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
            /*case R.id.action_logout:
                User.getCurrentUser(getContext()).logout();
                Toast.makeText(getHostActivity(), "Logout", Toast.LENGTH_LONG).show();
                break; */
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {

        if (resultCode == Constants.RESULT_RECEIVER_AVATAR_CREATE) {
            String imagePath = resultData.getString(Constants.Extras.IMAGE_PATH);
            int requestCode = resultData.getInt(LoadFileService.EXTRA_REQUEST_CODE);
            onReceiveAvatarCache(imagePath, requestCode);
        }
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        User user = User.getCurrentUser(getContext());

        int countUnread = 0;

        MessengerApplication.getInstance().displayAvatar(dataSnapshot.child(Constants.DataBase.PHOTO_URL).getValue(String.class), civAvatar);
        String photoUrl = dataSnapshot.child(Constants.DataBase.PHOTO_URL).getValue(String.class);

        if (photoUrl != null)
            user.setPhotoUri(Uri.parse(photoUrl));

        tvDisplayName.setText(dataSnapshot.child(Constants.DataBase.DISPLAY_NAME).getValue(String.class));
        user.setDisplayName(dataSnapshot.child(Constants.DataBase.DISPLAY_NAME).getValue(String.class));

        if ( dataSnapshot.hasChild(Constants.DataBase.UNREAD) ) {
            countUnread = (int) dataSnapshot.child(Constants.DataBase.UNREAD).getChildrenCount();
        }

        if ( dataSnapshot.hasChild(Constants.DataBase.CHATS)
                && dataSnapshot.hasChild(Constants.DataBase.LAST_MESSAGE) ) {

            Map<String, String> mapChats = (Map<String, String>) dataSnapshot.child(Constants.DataBase.CHATS).getValue();
            Map<String, String> mapLastMessages = (Map<String, String>) dataSnapshot.child(Constants.DataBase.LAST_MESSAGE).getValue();

            user.putChats(mapChats);
            user.putLastMessages(mapLastMessages);

        } else {

            user.getChats().clear();
            user.getLastMessages().clear();
        }

        MainMenuAdapter mainMenuAdapter = (MainMenuAdapter) rvMenu.getAdapter();
        mainMenuAdapter.setCountMessages(countUnread);
        mainMenuAdapter.notifyDataSetChanged();

        user.setUnread(countUnread);
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Toast.makeText(getContext(), databaseError.getMessage(), Toast.LENGTH_LONG).show();
    }

    public void capturePhoto(String targetFilename) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = new File( FileUtils.getFilesDir(), targetFilename);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));

        if (intent.resolveActivity(getHostActivity().getPackageManager()) != null) {
            startActivityForResult(intent, Constants.REQUEST_CODE_CAPTURE_PHOTO);
        }
    }

    protected void chooseFromGallery() {
        chooseFromGallery(Constants.REQUEST_CODE_CHOOSE_FROM_GALLERY);
    }

    protected void chooseFromGallery(int requestCode) {
        Intent intent = new Intent();

        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(intent, requestCode);
    }

    private Dialog createDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(R.array.image_actions, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (which == ITEM_CAPTURE_PHOTO)
                    capturePhoto(Calendar.getInstance().getTimeInMillis()+".jpg");
                else if (which == ITEM_CHOOSE_FROM_GALLERY)
                    chooseFromGallery();

                dialog.dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        cleanupListeners();
    }

    private void cleanupListeners() {

        //loadFileReceiver.setReceiver(null);

        if (userDataReference != null)
            userDataReference.removeEventListener(this);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(int id);
    }
}

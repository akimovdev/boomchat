package com.akimovdev.boomchat.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.MessengerFragment;

public class NotificationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        if (!intent.hasExtra(Constants.Extras.CHAT))
            throw new Error("Notification activity must have parcelable chat");

        if (!intent.hasExtra(Constants.Extras.CONTACT_USER))
            throw new Error("Notification activity must have parcelable contact user");

        User user = intent.getParcelableExtra(Constants.Extras.CONTACT_USER);
        Chat chat = intent.getParcelableExtra(Constants.Extras.CHAT);

        addFragment (
                MessengerFragment.newInstance (
                        chat,
                        User.getCurrentUser(this),
                        user ) );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }

        //return super.onOptionsItemSelected(item);
        return true;
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }
}

package com.akimovdev.boomchat.ui.fragments;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.akimovdev.boomchat.ui.activities.BaseActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class BaseFragment <ActivityClass extends BaseActivity> extends Fragment {

    private final static String TAG = "BaseFragment";

    private DatabaseReference connectedRef;
    private ValueEventListener connectListener;

    public ActivityClass getHostActivity() {
        //noinspection unchecked
        return (ActivityClass) getActivity();
    }

    protected final void startConnectListener() {

        connectedRef = getConnectedInfoRef();

        connectListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = dataSnapshot.getValue(Boolean.class);

                if (connected)
                    onConnected();
                else
                    onDisconnected();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.getMessage());
            }
        };

        connectedRef.addValueEventListener(connectListener);
    }

    protected final void stopConnectListener() {
        if ( connectedRef != null && connectListener != null )
            connectedRef.removeEventListener(connectListener);
    }

    protected void onConnected() {
        /* Nothing to do */
        Log.d(TAG, "onConnected()");
    }

    public void onDisconnected() {
        /* Nothing to do */
        Log.d(TAG, "onDisconnected()");
    }

    private DatabaseReference getConnectedInfoRef() {
        return FirebaseDatabase.getInstance().getReference(".info/connected");
    }

}


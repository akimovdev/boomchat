package com.akimovdev.boomchat.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.Chat;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.activities.MessengerActivity;
import com.akimovdev.boomchat.ui.activities.FriendsActivity;
import com.akimovdev.boomchat.ui.adapters.ChatAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatListFragment extends BaseFragment {

    private RecyclerView rvChatList;
    private TextView tvEmptyList;
    private ProgressBar progressBar;

    private FloatingActionButton fab;

    private OnListFragmentInteractionListener mListener;

    public ChatListFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getHostActivity().getSupportActionBar().setTitle(getString(R.string.title_activity_messages));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_chat_list, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        rvChatList = (RecyclerView) view.findViewById(R.id.list);
        tvEmptyList = (TextView) view.findViewById(R.id.tvEmptyList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getHostActivity());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);

        rvChatList.setLayoutManager(layoutManager);

        User currentUser = User.getCurrentUser(getContext());

        if (currentUser.getChats().isEmpty()) {
            tvEmptyList.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }

        mListener = new OnListFragmentInteractionListener() {
            @Override
            public void onListFragmentInteraction(Chat chat, User currentUser, User contactUser) {
                MessengerActivity.startNewChatActivity(getContext(), contactUser, chat);
            }
        };

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                .child(Constants.DataBase.CHATS).child(User.getCurrentUser(getContext()).getUid());

        ref.keepSynced(true);

        rvChatList.setAdapter(new ChatAdapter(view.getContext(), ref, mListener));

        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FriendsActivity.startChatFriendsActivity(getContext());
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        final ChatAdapter adapter = (ChatAdapter) rvChatList.getAdapter();
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                tvEmptyList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        });

        if (!getHostActivity().isNetworkConnected()) {
            tvEmptyList.setText(getString(R.string.no_internet_connection));
            progressBar.setVisibility(View.GONE);

            if (adapter.getItemCount() == 0)
                tvEmptyList.setVisibility(View.VISIBLE);

        } else {
            tvEmptyList.setText(getString(R.string.empty));
        }

        startConnectListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopConnectListener();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ChatAdapter adapter = (ChatAdapter) rvChatList.getAdapter();
        adapter.cleanupListener();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                getHostActivity().onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Chat chat, User currentUser, User contactUser);
    }
}

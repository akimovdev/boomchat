package com.akimovdev.boomchat.ui.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;

import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.fragments.SignInFragment;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class AuthorizationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_toolbar_overlay);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        if (savedInstanceState != null)
            return;

        addFragment(new SignInFragment());
    }


    public void saveUser(final FirebaseUser firebaseUser, final String email, final String password, final String displayName, final String photoUrl, final OnCompleteListener onCompleteListener) {

        if (firebaseUser == null)
            throw new Error("User is not authorized !");

        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(displayName).build();

        firebaseUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (!task.isSuccessful()) {
                    //throw new Error(task.getException().getMessage());
                }

                User user = User.createCurrentUser(MessengerApplication.getInstance().getApplicationContext(), firebaseUser, email, displayName, photoUrl, new HashMap<String, Boolean>(), new HashMap<String, String>(), new HashMap<String, String>(), password, 0);

                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();//.child(Constants.DataBase.USERS).child(firebaseUser.getUid());

                Map<String, Object> map = new HashMap<>();
                map.put("/"+Constants.DataBase.USERS+"/"+user.getUid()+"/"+Constants.DataBase.EMAIL, user.getEmail());
                map.put("/"+Constants.DataBase.USERS+"/"+user.getUid()+"/"+Constants.DataBase.DISPLAY_NAME, user.getDisplayName());
                map.put("/"+Constants.DataBase.USERS+"/"+user.getUid()+"/"+Constants.DataBase.PHOTO_URL, user.getPhotoUrl());
                map.put("/"+Constants.DataBase.USERS+"/"+user.getUid()+"/"+Constants.DataBase.UID, user.getUid());

                map.put("/"+Constants.DataBase.SEARCH+"/"+Constants.DataBase.USERS+"/"+user.getUid(), user.getSearchMetadata());
                mDatabase.updateChildren(map).addOnCompleteListener(onCompleteListener);

            }
        });
    }

    public String validateForm(String email, String password, String displayName) {

        String message = null;

        if (displayName != null && TextUtils.isEmpty(displayName))
            message = getString(R.string.alert_empty_display_name);

        else if (email == null || TextUtils.isEmpty(email))
            message = getString(R.string.alert_empty_email);

        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
            message = getString(R.string.alert_unavailable_email);

        else if (password == null || TextUtils.isEmpty(password))
            message = getString(R.string.alert_empty_password);

        else if (password.length() < 6)
            message = getString(R.string.alert_unavailable_password);

        return message;
    }

    @Override
    protected int getContainer() {
        return R.id.container;
    }
}

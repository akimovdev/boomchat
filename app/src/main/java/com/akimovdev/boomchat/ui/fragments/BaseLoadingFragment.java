package com.akimovdev.boomchat.ui.fragments;

import android.os.Handler;

import com.akimovdev.boomchat.Constants;

public abstract class BaseLoadingFragment extends BaseFragment {

    private Handler waitResponseHandler;
    private Runnable waitRunnable;


    @Override
    public void onStart() {
        super.onStart();
        startWaitResponse();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopWaitResponse();
    }

    private void startWaitResponse() {
        waitResponseHandler = new Handler();
        waitRunnable = new Runnable() {
            @Override
            public void run() {
                onTimeUpResponse();
            }
        };

        waitResponseHandler.postDelayed(waitRunnable, Constants.WAIT_SERVER_RESPONSE);
    }

    private void stopWaitResponse() {
        waitResponseHandler.removeCallbacks(waitRunnable);
    }

    protected final void doneWaitResponse() {
        if (waitRunnable != null && waitResponseHandler != null)
            waitResponseHandler.removeCallbacks(waitRunnable);
    }

    protected abstract void onTimeUpResponse();
}

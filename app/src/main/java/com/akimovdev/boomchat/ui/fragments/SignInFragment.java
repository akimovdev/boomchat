package com.akimovdev.boomchat.ui.fragments;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akimovdev.boomchat.ui.activities.MainActivity;
import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.MessengerApplication;
import com.akimovdev.boomchat.model.User;
import com.akimovdev.boomchat.ui.activities.AuthorizationActivity;
import com.akimovdev.boomchat.R;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
//import com.google.api.server.spi.request.Auth;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.HashMap;
import java.util.Map;

public class SignInFragment extends BaseFragment<AuthorizationActivity>  implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = "SignInFragment";

    private EditText etEmail;
    private EditText etPassword;
    private TextView tvSingUp;
    private Button buttonSignIn;
    private RelativeLayout rlLoginGoogle;
    private FirebaseAuth mAuth;
    private GoogleApiClient googleApiClient;
    private boolean isAuthStart = false;

    ValueEventListener userEventListener;
    DatabaseReference userRef;

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                ///.requestIdToken("AIzaSyAq9AkklCoqAM3OobuZ7UjtKGfgnf2ZH_c")
                .requestIdToken(getString(R.string.default_web_client_id))
                .build();

        googleApiClient = new GoogleApiClient.Builder(getHostActivity())
                .enableAutoManage(getHostActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signin, container, false);

        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        tvSingUp = (TextView) view.findViewById(R.id.tvSignUp);

        rlLoginGoogle = (RelativeLayout) view.findViewById(R.id.rlLoginGoogle);
        rlLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authWithGoogle();
            }
        });


        buttonSignIn = (Button) view.findViewById(R.id.buttonSignIn);
        buttonSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signIn(etEmail.getText().toString(), etPassword.getText().toString());
            }
        });

        tvSingUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHostActivity().replaceFragment(new SignUpFragment(), true);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getHostActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        startConnectListener();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopConnectListener();
    }

    @Override
    protected void onConnected() {
        super.onConnected();
    }

    @Override
    public void onDisconnected() {
        super.onDisconnected();
        if (isAuthStart)
            onAuthFail(getString(R.string.alert_chek_internet_available));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == Constants.REQUEST_CODE_AUTH_GOOGLE) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
                    GoogleSignInAccount account = result.getSignInAccount();
                    firebaseAuthWithGoogle(account);
                } else {
                    onAuthFail(getString(R.string.alert_auth_internet_not_available));
                }
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            if (requestCode == Constants.REQUEST_CODE_AUTH_GOOGLE) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                //onAuthFail(getString(R.string.alert_auth_internet_not_available));
            }
        }

    }

    private void authWithGoogle() {

        if (!getHostActivity().isNetworkConnected()) {
            onAuthFail(getString(R.string.alert_sending_package_disable));
            return;
        }

        Intent authorizeIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(authorizeIntent, Constants.REQUEST_CODE_AUTH_GOOGLE);
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);

        getHostActivity().showProgressDialog();

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(getHostActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<AuthResult> task) {

                        if (!task.isSuccessful()) {

                            String message = null;

                            if (task.getException()!= null && task.getException().getMessage()!=null)
                                message = task.getException().getMessage();
                            else
                                message = getString(R.string.alert_auth_internet_not_available);

                            onAuthFail(message);

                        } else {
                            final FirebaseUser firebaseUser = task.getResult().getUser();
                            initUser(firebaseUser, account);
                            //saveUser(firebaseUser, account);
                        }
                    }
                });
    }

    private void saveUser(final FirebaseUser firebaseUser, GoogleSignInAccount account) {

        String photoUrl = account.getPhotoUrl().toString();

        if (photoUrl.contains("s96-c"))
            photoUrl = photoUrl.replace("s96-c", "s196-c");

        getHostActivity().saveUser(firebaseUser, getString(R.string.via_google_plus), null, account.getDisplayName(), photoUrl, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                getHostActivity().hideProgressDialog();

                if (!task.isSuccessful()) {
                    Toast.makeText(getHostActivity(), "Record error", Toast.LENGTH_LONG).show();
                } else {
                    sendUID(firebaseUser.getUid());
                }
            }
        });
    }


    private void sendUID(final String uid) {

        RequestQueue queue = Volley.newRequestQueue(getContext());
        //String url ="http://s673212913.onlinehome.us/senduid";
        String url = Constants.URL_SERVER +"senduid.php";


        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        getHostActivity().hideProgressDialog();
                        isAuthStart = false;
                        startActivity(new Intent(getActivity(), MainActivity.class));
                        getActivity().finish();
                    }


                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getHostActivity().hideProgressDialog();
                        onAuthFail(getString(R.string.alert_auth_internet_not_available));
                    }
                }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        String androidId = MessengerApplication.getAndroidID(getContext().getContentResolver());
                        //String androidId = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);

                        Map<String, String>  params = new HashMap<String, String>();

                        params.put("uid", uid);
                        params.put("duid", androidId);

                        return params;
                    }
                };

        queue.add(stringRequest);
    }

    private void initUser(final FirebaseUser firebaseUser, final GoogleSignInAccount account) {

        userRef = FirebaseDatabase.getInstance().getReference().child(Constants.DataBase.USERS).child(firebaseUser.getUid());
        userEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot == null || dataSnapshot.getValue() == null) {

                    if (account == null) {
                        onAuthFail("Fail of load data, maybe user was removed");
                        return;
                    }

                    saveUser(firebaseUser, account);
                    return;
                }

                String email = dataSnapshot.child(Constants.DataBase.EMAIL).getValue(String.class);
                String displayName = dataSnapshot.child(Constants.DataBase.DISPLAY_NAME).getValue(String.class);
                String password = dataSnapshot.child(Constants.DataBase.PASSWORD).getValue(String.class);
                String photoUrl = dataSnapshot.child(Constants.DataBase.PHOTO_URL).getValue(String.class);
                Map<String, Boolean> friends = (Map<String, Boolean>) dataSnapshot.child(Constants.DataBase.FRIENDS).getValue();
                Map<String, String> chats = (Map<String, String>) dataSnapshot.child(Constants.DataBase.CHATS).getValue();
                Map<String, String> lastMessages = (Map<String, String>) dataSnapshot.child(Constants.DataBase.LAST_MESSAGE).getValue();

                int countUnread = 0;

                if ( dataSnapshot.hasChild(Constants.DataBase.UNREAD) ) {
                    countUnread = (int) dataSnapshot.child(Constants.DataBase.UNREAD).getChildrenCount();
                }

                User.createCurrentUser(MessengerApplication.getInstance().getApplicationContext(),
                        firebaseUser,
                        email,
                        displayName,
                        photoUrl,
                        friends,
                        chats,
                        lastMessages,
                        password,
                        countUnread);

                sendUID(firebaseUser.getUid());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                getHostActivity().hideProgressDialog();
                onAuthFail(databaseError.getMessage());
            }
        };

        userRef.addListenerForSingleValueEvent(userEventListener);
    }

    private void signIn(String email, String password) {

        String validMessage = getHostActivity().validateForm(email, password, null);
        if (validMessage != null) {
            Toast.makeText(getContext(), validMessage, Toast.LENGTH_SHORT).show();
            return;
        }

        if (!getHostActivity().isNetworkConnected()) {
            onAuthFail(getString(R.string.alert_sending_package_disable));
            return;
        }

        getHostActivity().showProgressDialog();

        isAuthStart = true;
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnSuccessListener(getHostActivity(), new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        initUser(authResult.getUser(), null);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        getHostActivity().hideProgressDialog();
                        onAuthFail(e.getMessage());
                    }
                });
    }

    private void onAuthFail(String alert) {

        if (FirebaseAuth.getInstance().getCurrentUser() != null)
            FirebaseAuth.getInstance().signOut();

        Auth.GoogleSignInApi.signOut(googleApiClient);
        isAuthStart = false;
        Log.d(TAG, "Fail: "+alert);
        getHostActivity().hideProgressDialog();
        getHostActivity().showAlertDialog(getString(R.string.alert_auth_failed), alert);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
}

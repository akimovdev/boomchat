package com.akimovdev.boomchat.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.R;
import com.akimovdev.boomchat.model.MenuItem;
import com.akimovdev.boomchat.ui.fragments.DashboardFragment;

import java.util.ArrayList;
import java.util.List;


public class MainMenuAdapter extends RecyclerView.Adapter<MainMenuAdapter.MenuViewHolder> {

    private List<MenuItem> menuItemList;
    private DashboardFragment.OnListFragmentInteractionListener listener;

    private int countMessages = 0;

    int[] menuIds = new int[]{ Constants.MainMenu.ITEM_MESSAGES,
            Constants.MainMenu.ITEM_FRIENDS,
            Constants.MainMenu.ITEM_SHARE,
            Constants.MainMenu.ITEM_REQUEST,
            Constants.MainMenu.ITEM_SETTINGS,
            Constants.MainMenu.ITEM_EXIT };

    public MainMenuAdapter(Context context, int countMessages, DashboardFragment.OnListFragmentInteractionListener listener) {
        createMenuList(context);
        this.listener = listener;
        this.countMessages = countMessages;
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_menu, parent, false);

        return new MainMenuAdapter.MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MenuViewHolder holder, final int position) {
        MenuItem item = menuItemList.get(position);

        if (position == 0 && countMessages > 0) {
            holder.tvCount.setText(String.valueOf(countMessages));
            holder.tvCount.setVisibility(View.VISIBLE);
        } else {
            holder.tvCount.setVisibility(View.GONE);
        }

        holder.ivIcon.setImageResource(item.icon);
        holder.tvItem.setText(item.item);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.onListFragmentInteraction(menuIds[position]);

            }
        });
    }

    @Override
    public int getItemCount() {
        return menuItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return menuIds[position];
    }

    static class MenuViewHolder extends RecyclerView.ViewHolder {

        ImageView ivIcon;
        TextView tvItem;
        TextView tvCount;// = (TextView) holder.itemView.findViewById(R.id.tvCount);

        public MenuViewHolder(View itemView) {
            super(itemView);

            ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
            tvItem = (TextView) itemView.findViewById(R.id.tvItem);
            tvCount = (TextView) itemView.findViewById(R.id.tvCount);
        }
    }

    public void setCountMessages(int countMessages) {
        this.countMessages = countMessages;

    }

    private void createMenuList(Context context) {
        menuItemList = new ArrayList<>();

        MenuItem menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_message;
        menuItem.item= context.getString(R.string.item_messages);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_person_36;
        menuItem.item= context.getString(R.string.item_contacts);
        menuItemList.add(menuItem);


        menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_share_24;
        menuItem.item= context.getString(R.string.item_share_inchat);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_info_black_36px;
        //menuItem.icon = R.drawable.ic_bomb;
        menuItem.item= context.getString(R.string.item_about);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_feedback_36;
        menuItem.item= context.getString(R.string.item_feedback);
        menuItemList.add(menuItem);

        menuItem = new MenuItem();
        menuItem.icon = R.drawable.ic_exit_36;
        menuItem.item= context.getString(R.string.item_exit);
        menuItemList.add(menuItem);
    }

}

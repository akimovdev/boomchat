package com.akimovdev.boomchat.ui.popup;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.PopupWindow;

public class BasePopupWindow extends PopupWindow {

    protected BasePopupWindow (View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);

        setOutsideTouchable(true);
        setFocusable(focusable);
    }

    public void showAtLocation(int gravity, int x, int y) {
        super.showAtLocation(getContentView(), gravity, x, y);
    }

    static DisplayMetrics getDisplaySize(Activity context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        context.getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);

        return displayMetrics ;
    }
}

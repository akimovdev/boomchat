package com.akimovdev.boomchat.components.loader;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.util.Log;

import com.akimovdev.boomchat.Constants;
import com.akimovdev.boomchat.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ImageLoader implements DataLoader {

    private static final int MAX_IMAGE_DIMENSION = 720;

    private Uri uri;
    private User user;
    private int requestCode;
    private Handler mHandler;
    private Context context;
    private LoadListener loadListener;

    public interface LoadListener {
        //void onCreatePublication(Publication publication);
        void onCreateAvatar(String downloadUrl, int requestCode);
    }

    public static ImageLoader newImageLoader(Context context, Uri uri, int requestCode, User user, LoadListener loadListener) {
        return new ImageLoader(context, uri, requestCode, user, loadListener);
    }

    private ImageLoader(Context context, Uri uri, int requestCode, User user, LoadListener loadListener) {
        this.uri = uri;
        this.requestCode = requestCode;
        this.user = user;
        this.loadListener = loadListener;
        this.context = context;
        mHandler = new Handler(context.getMainLooper());
    }

    protected byte[] compress(Uri uriFile) {

        byte[] bytes = new byte[0];

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uriFile);

            int width = bitmap.getWidth();

            if (width > MAX_IMAGE_DIMENSION) {
                float ratio = (float) MAX_IMAGE_DIMENSION / (float) width;

                width = MAX_IMAGE_DIMENSION;
                int height = (int)((float)bitmap.getHeight() * ratio);

                bitmap = Bitmap.createScaledBitmap(bitmap, width, height, false);
            }

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
            bytes = stream.toByteArray();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return bytes;
    }

    private String getPath(Uri uri) {
        String path = uri.getPath();
        Log.d("TAG_PREVIEW", "getPath(): "+path);

        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {

            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } else
            return null;
    }


    private void saveUser() {

        Map<String, String> chatsMap = user.getChats();
        Set<String> uidSet = chatsMap.keySet();

        Map<String, Object> map = new HashMap<>();

        if (user.getPhotoUrl() != null) {

            map.put("/"+Constants.DataBase.USERS+"/"+user.getUid()+"/"+Constants.DataBase.PHOTO_URL, user.getPhotoUrl());

            for (String uid : uidSet) {
                String chatKey = chatsMap.get(uid);
                map.put("/"+Constants.DataBase.CHATS+"/"+uid+"/"+chatKey+"/"+Constants.DataBase.PHOTO_URL, user.getPhotoUrl());
            }

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.updateChildren(map);
        }

    }

    private void uploadFileToStorage(final Uri uriFile, String fileName, final OnSuccessListener<UploadTask.TaskSnapshot> listener) {
        byte[] compressImage = compress(uriFile);

        if (compressImage.length == 0) {
            return;
        }

        StorageMetadata metadata = new StorageMetadata.Builder().setContentType("image/jpeg").build();
        StorageReference storageRoot = FirebaseStorage.getInstance().getReferenceFromUrl(Constants.FIREBASE_STORAGE);

        final UploadTask uploadTask
                = storageRoot
                .child(Constants.DataStorage.STORAGE_IMAGE+"/"+fileName+"_SERVICE")
                .putBytes(compressImage, metadata);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                uploadTask.addOnSuccessListener(listener).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.getMessage();
                        e.getMessage();
                    }
                });
            }
        });
    }

    protected void avatar(Uri imageUri, final int requestCode) {

        String userName = user.getUid();


        uploadFileToStorage(imageUri, userName, new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Uri downloadUrl = taskSnapshot.getMetadata().getDownloadUrl();

                if ( requestCode == Constants.REQUEST_CODE_CAPTURE_PHOTO ||
                        requestCode == Constants.REQUEST_CODE_CHOOSE_FROM_GALLERY )
                    user.setPhotoUri(downloadUrl);

                saveUser();

                if (loadListener != null)
                    loadListener.onCreateAvatar(downloadUrl.toString(), requestCode);
            }
        });
    }

    @Override
    public void actionLoad() {
        if ( requestCode == Constants.REQUEST_CODE_CAPTURE_PHOTO ||
                requestCode == Constants.REQUEST_CODE_CHOOSE_FROM_GALLERY )
            avatar(uri, requestCode);
    }
}

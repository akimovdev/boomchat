package com.akimovdev.boomchat.utils;

import java.util.HashMap;
import java.util.Map;

public class PoolTransporter  {

    private static PoolTransporter instance;

    private Map<String,Object> map = new HashMap<>();


    public static PoolTransporter getInstance() {
        if (instance == null)
            instance = new PoolTransporter();

        return instance;
    }

    private PoolTransporter() {}

    public void push(String key, Object object) {
        map.put(key, object);
    }

    public Object pop(String key) {
        return map.remove(key);
    }

    public Object find(String key) {
        return map.get(key);
    }

}
